<?php                                           // offer-to-close\common_library_private\src\Models\ContactForm.php
namespace offer_to_close\common_library_private\Models;
use Illuminate\Database\Eloquent\Model;
class ContactForm extends Model
{
    protected $guarded = [];
    protected $table = 'OtcUsers';
    protected $connection = 'mysql-admin';
}