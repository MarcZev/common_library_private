<?php

namespace offer_to_close\common_library_private\Library;

/*******************************************************************************
 * Class _Arrays
 *
 * Author: Marc Zev
 * Development Date: Aug 26, 2013
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/

class _Arrays
{
//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function displayAsTable($array, $useKey=false, $class=null, $id=null, $fieldList=array())
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function displayAsTable($array, $useKey = false, $class = null, $id = null, $fieldList = array(), $orientation = 'h')
    {
        $tbl              = null;
        $orientHorizontal = true;

        if (strtolower(substr($orientation, 0, 1)) == 'v') $orientHorizontal = false;

        if (!is_array($array) || count($array) < 1) return (null);

        if (empty($class))
        {
            $classDelimiter = null;
        }
        else $classDelimiter = "-";

        $classTable  = $class . $classDelimiter . "table";
        $classHeader = $class . $classDelimiter . "header";
        $classRow    = $class . $classDelimiter . "row";


        if (empty($class))
        {
            $idDelimiter = null;
        }
        else $idDelimiter = "-";

        $idTable  = $id . $idDelimiter . "table";
        $idHeader = $id . $idDelimiter . "header";
        $idRow    = $id . $idDelimiter . "row";

        if (count($fieldList) == 0)
        {
            $fieldList = array_keys($array[0]);
        }

        if ($orientHorizontal)
        {
            $tbl .= "<table class='{$classTable}' id='{$idTable}'>" . "\n";

            $tbl .= " <tr  class='{$classHeader}' id='{$idHeader}'>" . "\n";
            foreach ($fieldList as $fld)
            {
                $classHCell = $classHeader . "-${fld}";
                $idHCell    = $idHeader . "-{$fld}";
                $tbl        .= "   <th class='{$classHCell}' id='{$idHCell}'>{$fld}</th>" . "\n";
            }
            $tbl .= " </tr>" . "\n";

            foreach ($array as $key => $row)
            {
                $tbl .= " <tr  class='{$classRow}' id='{$idRow}'>" . "\n";
                foreach ($fieldList as $fld)
                {
                    $classRCell = $classRow . "-{$fld}";
                    $idRCell    = $idRow . "-{$fld}";
                    $tbl        .= "   <td class='{$classRCell}' id='{$idRCell}'>" . (isset($row[$fld]) ? $row[$fld] : '') . "</td>" . "\n";
                }
                $tbl .= " </tr>" . "\n";
            }

            $tbl .= "</table> <!-- end .{$class} #{$id} -->" . "\n";
        }
        else
        {
            $tbl .= "<table class='{$classTable}' id='{$idTable}'>" . "\n";

            foreach ($array as $key => $row)
            {
                foreach ($fieldList as $fld)
                {
                    $classHCell = $classHeader . "-{$fld}";
                    $idHCell    = $idHeader . "-{$fld}";
                    $classRCell = $classRow . "-{$fld}";
                    $idRCell    = $idRow . "-{$fld}";
                    $tbl        .= " <tr  class='{$classRow}' id='{$idRow}'>" . "\n";
                    $tbl        .= "   <th class='{$classHCell}' id='{$idHCell}'>{$fld}</th>" . "\n";
                    $tbl        .= "   <td class='{$classRCell}' id='{$idRCell}'>" . (isset($row[$fld]) ? $row[$fld] : '') . "</td>" . "\n";
                    $tbl        .= " </tr>" . "\n";
                }
            }
            $tbl .= "</table> <!-- end .{$class} #{$id} -->" . "\n";
        }

        return ($tbl);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// removeColumn
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function removeColumn($array, $columnName)
    {
        foreach ($array as &$row)
        {
            unset($row[$columnName]);
        }
        return $array;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // searchByColumn
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function searchByColumn(&$array, $columnName, $searchValue)
    {
        try
        {
            $col = array_column($array, $columnName);
            $row = array_search($searchValue, $col);
            if ($row === false) return false;

            return $array[$row] ?? false;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // sortByColumn
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function sortByColumn(&$array, $columnName, $sortOrder = SORT_ASC)
    {
        if (!array_multisort(array_column($array, $columnName), $sortOrder, $array)) ddd(['MultiSort failed', $array,
                                                                                         __METHOD__=>__LINE__]);

    }
    static public function sortByColumn_obsolete(&$array, $columnName, $sortOrder = SORT_ASC) //todo: delete
    {
        $sort_col = [];
        foreach ($array as $key => $row)
        {
            $sort_col[$key] = $row[$columnName];
        }
        if (!array_multisort($sort_col, $sortOrder, $array))
        {
            ddd(['MultiSort failed', $array, __METHOD__ => __LINE__]);
        }
    }

    static public function sortByTwoColumns(&$array, $columnName1, $sortOrder1 = SORT_ASC,
                                                     $columnName2, $sortOrder2 = SORT_ASC)
    {
        /*
        ddd([$columnName1, count(array_column($array, $columnName1)),
                   $columnName2, count(array_column($array, $columnName2)),
                   array_keys(reset($array))]);
        */
        try
        {
            array_multisort(array_column($array, $columnName1), $sortOrder1,
                array_column($array, $columnName2), $sortOrder2, $array);
        }
        catch (\Exception $e)
        {        }
    }

//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function writeToFile($file, $array)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function writeToFile($file, $array)
    {
        if (empty($file) || empty($array)) return (false);

        $fh = @fopen($file, "w")
        or die ("Can't open file (and your little dog too.");

        if (!$fh)
        {
            _Debug::error("The File Handle cannot be found!");
            return (FALSE);
        }

        fputs($fh, var_export($array, TRUE));
        fclose($fh);
        return (true);
    }

//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function writeToIniFile($file, $array)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function writeToIniFile($file, $array, $tStamp = true)
    {
        if (empty($file) || empty($array)) return (false);

        $fh = @fopen($file, "w")
        or die ("Can't open file (and your little dog too.");

        if (!$fh)
        {
            _Debug::error("The File Handle cannot be found!");
            return (FALSE);
        }

        foreach ($array as $key => $val)
        {
            if (is_array($val))
            {
                fputs($fh, "\r\n");
                fputs($fh, "[" . $key . "]");
                fputs($fh, "\r\n");
                foreach ($val as $k => $v)
                {
                    (is_int($v)) ? $q = "" : $q = '"';
                    fputs($fh, $k . " = $q" . $v . $q);
                    fputs($fh, "\r\n");
                }
            }
            else
            {
                (is_int($val)) ? $q = "" : $q = '"';
                fputs($fh, $key . " = $q" . $val . $q);
                fputs($fh, "\r\n");
            }
        }

        if ($tStamp)
        {
            fputs($fh, "\r\n");
            $today = date("m/d/y H:m:s");
            fputs($fh, ";; Last Updated: " . $today);
        }

        fclose($fh);
        return (true);
    }

//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function writeToString($ay)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function writeToString($ay, $name = "")
    {
        $retval = "";
        foreach ($ay as $key => $val)
        {
            $retval .= $key . " = " . $val;
            $retval .= "\n";
            if (is_array($val))
            {
                $retval .= "[" . $key . "]";
                $retval .= "\n";
                foreach ($val as $k => $v)
                {
                    $retval .= "      " . $k . " = " . $v;
                    $retval .= "\n";
                }
            }
        }
        return ($retval);
    }

//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function writeToString($ay)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function prefixIndex($prefix, $ay)
    {
        if (empty($prefix)) return $ay;
        $rv = [];
        foreach ($ay as $key => $val)
        {
            $rv[$prefix . $key] = $val;
        }
        return ($rv);
    }

    /**
     * @param array $input
     * @param array $defaults
     *
     * @return array
     */
    public static function assignDefaults(array $input, array $defaults) :array
    {
        if (count($defaults) == 0) return $input;
        if (count($input) == 0) return $defaults;
        $rv = [];

        foreach ($defaults as $key=>$value)
        {
            $input[$key] = $input[$key] ?? $value;
        }

        return $input;
    }
    public static function renameColumns(array $matrix, array $columnList)
    {
        if (!is_array(reset($matrix))) return false;

        foreach($matrix as $idx=>$row)
        {
            if (!is_array($row)) continue;
            foreach($columnList as $old=>$new)
            {
                if (!isset($matrix[$idx][$old])) continue;
                $matrix[$idx][$new] = $matrix[$idx][$old];
                unset($matrix[$idx][$old]);
            }
        }
        return $matrix;
    }
}