<?php
namespace offer_to_close\common_library_private\Library;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Toumai
 *
 * This class is the ancestor from which all other classes beckon forth. It is named for the oldest hominin ancestor.
 * line 2
 *
 * @author  Marc Zev
 *
 * @version 2.0.0.2015
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Toumai
{
// properties
    private $data = array();
    private $thisClass = false;
    private $thisCode = false;
    private $thisCodeMethods = false;
    private $thisClassMethods = false;
    private $thisClassMethodParameters = false;
    private $thisClassMethodModifiers = false;
    private $thisClassMethodDocBlock = false;

    private $docBlockTags = array('abstract'   => false, 'access' => false, 'author' => 'Author: ',
                                  'category'   => false, 'copyright' => false,
                                  'deprecated' => false,
                                  'example'    => false,
                                  'final'      => false, 'filesource' => false,
                                  'global'     => 'Global: ',
                                  'ignore'     => false, 'internal' => false,
                                  'license'    => false, 'link' => false,
                                  'method'     => false,
                                  'name'       => false,
                                  'package'    => false, 'param' => 'Parameter: ', 'property' => false,
                                  'return'     => false,
                                  'see'        => false, 'since' => false, 'static' => false, 'staticvar' => false, 'subpackage' => false,
                                  'todo'       => false, 'tutorial' => false,
                                  'uses'       => false,
                                  'var'        => false, 'version' => 'Version: ',);

    private $inlineTags = array('example' => false,
                                'id'      => false, 'internal' => false, 'inheritdoc' => false,
                                'link'    => false,
                                'source'  => false,
                                'toc'     => false, 'tutorial' => false,);

    function __construct()
    {
//	echo "Yay";
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * disectDocBlock
     *
     * Decompose a doc block into its parts and tags
     *
     * @document return The indices of the array that gets returned
     *
     * @author   Marc Zev
     *
     * @version  2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function disectDocBlock($docBlock)
    {
        $return = array('title' => false, 'description' => false, 'tags' => array(),);

        if (!is_array($docBlock)) $docBlock = explode(chr(10), $docBlock);

        $title     = $description = false;
        $more      = $tags = $outline = array();
        $nullFound = true;
        $oIndex    = 0;

        foreach ($docBlock as $idx => &$line)
        {
            $line = trim($line);
            if (substr($line, 0, 3) == '/**')
            {
                unset($docBlock[$idx]);
                continue;
            }
            else
            {
                if (substr($line, 0, 2) == '*/')
                {
                    unset($docBlock[$idx]);
                    continue;
                }
                else
                {
                    if ($line == '*')
                    {
                        $line = null;
                    }
                    else if (substr($line, 0, 2) == '* ') $line = substr($line, 2);
                }
            }


            if (substr($line, 0, 1) == '@')
            {
                $tags[] = $line;
                continue;
            }
            else
            {
                if (is_null($line))
                {
                    $nullFound = true;
                }
                else
                {
                    if ($nullFound)
                    {
                        ++$oIndex;
                        $outline[$oIndex] = $line;
                    }
                    else
                    {
                        $outline[$oIndex] .= ' ' . $line;
                    }

                    $nullFound = false;
                }
            }

            if (!is_null($line))
            {
                if (substr($line, 0, 1) != '@')
                {
                    if (!$title)
                    {
                        $title = $line;
                    }
                    else
                    {
                        if (!$description)
                        {
                            $description = $line;
                        }
                        else $more[] = $line;
                    }
                }
                else $tags[] = $line;
            }
        }

        $return = array('title'       => isset($outline[1]) ? $outline[1] : null,
                        'description' => isset($outline[2]) ? $outline[2] : null,
                        'tags'        => $tags,);

        return $return;
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * document
     *
     * Used to dynamicaly create documention for any class extended from Toumai
     *
     * @author  Marc Zev
     *
     * @version 2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function document($method = '*')
    {

        $thisClass = $this->getThisClass();

        $docClass           = $thisClass->getDocComment();
        $disectedClassBlock = $this->disectDocBlock($docClass);

        $rMethod = false;

        echo "<h1>Class: $thisClass->name</h1>" . "\n";
        if (!empty($disectedClassBlock['description'])) echo "<h2  style='color: black; margin: 0 20px;'>" . $disectedClassBlock['description'] . "</h2>" . "\n";
        echo "<h3 style='text-align:center;'>Source: " . $thisClass->getFileName() . "</h3><br/>" . "\n";
//		echo "<div class='shadow' style='padding: 10px;'><pre>" . $docClass . "</pre></div>" . "\n";
        echo "<div class='shadow' style='padding: 10px;; background-color:#ffe;'>";
        echo "<h4>Doc-Block</h4><blockquote>";

        foreach ($this->docBlockTags as $tag => $label)
        {
            if (!$label) continue;
            $found = false;
            foreach ($disectedClassBlock['tags'] as $tagLine)
            {
                if (substr($tagLine, 0, strlen('@' . $tag)) == '@' . $tag)
                {
                    $found = true;
                    if ($found) $tag = trim(substr($tagLine, strlen('@' . $tag)));
                    break;
                }
            }
            if ($found) echo "{$label} {$tag}<br/>" . "\n";
        }
        echo "</blockquote></div>" . "\n";

        if (empty($method) || $method == '*')
        {
            $thisClassMethods = $this->thisClassMethods;
        }
        else
        {
            if (($thisClassMethod = $this->getThisClassMethod($method)))
            {
                $thisClassMethods = array($thisClassMethod);
            }
            else
            {
                $thisClassMethods = array();
                _Print::alert("`{$method}` is not a valid method for this class.");
            }
        }

//
// ... Document all the methods
//
        foreach ($thisClassMethods as $methodName => $m)
        {
            if (empty($m)) continue;

            $docMethod           = $this->thisClassMethodDocBlock[$methodName];  // Method's Doc Block
            $disectedMethodBlock = $this->disectDocBlock($docMethod);

            echo "<h2>Method: $methodName</h2>" . "\n";
            if (!empty($disectedMethodBlock['description'])) echo "<h3  style='color: black; margin: 0 20px;'>" . $disectedMethodBlock['description'] . "</h3>" . "\n";
            echo "<div class='shadow' style='padding: 10px; background-color:#eee;'>";
            echo "<h4>Doc-Block</h4><blockquote>";

            foreach ($this->docBlockTags as $tag => $label)
            {
                if (!$label) continue;
                $found = false;
                foreach ($disectedMethodBlock['tags'] as $tagLine)
                {
                    if (substr($tagLine, 0, strlen('@' . $tag)) == '@' . $tag)
                    {
                        $found = true;
                        if ($found) $tag = trim(substr($tagLine, strlen('@' . $tag)));
                        break;
                    }
                }
                if ($found) echo "{$label} {$tag}<br/>" . "\n";
            }
            echo "</blockquote></div>" . "\n";

            $methodParams = $this->thisClassMethodParameters[$methodName]; // Method's Parameters

            if (count($methodParams) > 0)
            {
                echo "<h4>Method Parameters:</h4><blockquote>" . "\n";

                foreach ($methodParams as $p)
                {
                    if (!is_object($p)) continue;

                    $l = array();

                    if ($p->isArray())
                    {
                        $ay = 'array ';
                    }
                    else  $ay = null;

                    if (method_exists($p, 'getText') && !is_null($p->getType()))
                    {
                        $l[] = $ay . $p->getType();
                    }
                    else  $l[] = $ay . 'mixed';

                    if ($p->isOptional())
                    {
                        $b1 = '[';
                        $b2 = ']';
                    }
                    else  $b1 = $b2 = null;

                    $l[] = $b1 . $p->name . $b2;

                    if ($p->isDefaultValueAvailable())
                    {
                        $q  = '`';
                        $dv = $p->getDefaultValue();
                        if (is_array($dv))
                        {
                            $t = array();
                            $s = array();
                            foreach ($dv as $k => $v)
                            {
                                if (is_null($v)) $v = 'NULL';
                                if (is_bool($v)) $v = ($v ? 'TRUE' : 'FALSE');
                                $t[] = $k . '=>' . $v;
                            }
                            $dv = 'ARRAY (' . implode(', ', $t) . ')';
                            $q  = null;
                        }

                        if (is_bool($dv))
                        {
                            $dv = $dv ? 'TRUE' : 'FALSE';
                            $q  = null;
                        }

                        $l[] = '(Default = ' . $q . $dv . $q . ')';
                    }


                    echo implode(' ', $l) . "<br/>\n";
                }
                echo "</blockquote>" . "\n";
            }

            $code = array();

// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
// ... The @document tag in the DocBlock is custom for this code
// ... The syntax is "@document var1 [[[var2] [var3] .... ]
// ... Where var1, var2, etc are the names of variables that should be documented when in the render
//
            $documentVarList = array();
            if (!empty($docMethod))
            {
//
//	... Collect the list of variables that need to be documented
//
                if (($s = stripos($docMethod, '@document ')) !== false)
                {
                    $rem = substr($docMethod, $s);
                    $l   = explode("\n", $rem);
                    foreach ($l as $i => $line)
                    {
                        if (stripos($line, '@document ') !== false)
                        {
                            list(, $dv) = explode('@document', $line);
                            list($dv, $v, $dd) = explode(' ', $line, 3);
                            $documentVarList[$v] = $dd;
                        }
                    }
                }

//
// ... If variables need to be documented then get the source code and read the variable definitions
//
                if (count($documentVarList) > 0)
                {
                    echo "<h4>Method Variable Definitions:</h4><blockquote>" . "\n";

                    if (count($code) == 0)
                    {
                        $code = $this->getThisCode();
                    }
                    else reset($code);

                    $methodFound = false;
                    foreach ($code as $line)
                    {
                        if (strpos($line, $m->name) === false ||
                            stripos($line, implode('|', $this->thisClassMethodModifiers[$methodName])) === false)
                        {
                            continue;
                        }
                    }

                    if (count($documentVarList) > 0)
                    {
                        $methodSource = $this->getThisCodeMethod($methodName);

                        foreach ($documentVarList as $var => $def)
                        {
                            $varDef = $this->getVariableDefinition($var, $def, $methodSource, 'table');
                            echo $varDef;
                        }

                    }
                    else
                    {
                    }

                    echo "</blockquote>" . "\n";

                }


            }
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        }

    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * findSet
     *
     * Finds the first set of string that is properly nested using a matching pair of delimiters. The
     * delimiters are () {} <> & {}
     *
     * @param string $string      String in which to find set
     * @param string $chrOpen     The opening character of the set delimiter '('|'['|'<'|'{'
     * @param bool   $includeEnds If TRUE, the returned string will include the leading and ending delimeters
     * @param mixed  $details     Returns the various values that describe where the set is
     *
     * @document details The indices of the array that gets assigned to $details
     *
     * @return string|bool Value within the set or FALSE
     *
     * @author   Marc Zev
     *
     * @version  2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function findSet($string, $chrOpen = '(', $includeEnds = true, &$details = array())
    {
        return self::_findSet($string, $chrOpen, $includeEnds,$details);
    }
    public static function _findSet($string, $chrOpen = '(', $includeEnds = true, &$details = array())
    {
        $details = array('originalLength' => false, 'setStart' => false, 'setEnd' => false, 'setLength' => false, 'includeEnds' => $includeEnds);
        $chrSets = array('(' => ')', '[' => ']', '<' => '>', '{' => '}', '`'=>'`');
        $pos     = -1;
        $cnt     = 0;

        $chrs = str_split($string, 1);

        $details['originalLength'] = count($chrs);

        $final = null;
        $store = false;
        foreach ($chrs as $c)
        {
            ++$pos;

            if ($c == $chrOpen)
            {
                if ($cnt == 0)
                {
                    $store = true;
                    if ($includeEnds)
                    {
                        $final               .= $chrOpen;
                        $details['setStart'] = $pos;
                    }
                    else $details['setStart'] = $pos + 1;

                }
                else  $final .= $c;

                ++$cnt;
                continue;
            }
            else
            {
                if ($c == $chrSets[$chrOpen])
                {
                    --$cnt;
                    if ($cnt == 0)
                    {
                        $store = false;
                        if ($includeEnds)
                        {
                            $final             .= $c;
                            $details['setEnd'] = $pos;
                        }
                        else $details['setEnd'] = $pos - 1;
                        break;
                    }
                    else $final .= $c;
                    continue;
                }
                else if ($store) $final .= $c;
            }
        }

        $details['setLength'] = strlen($final);

        if ($cnt == 0)
        {
            return $final;
        }
        else return false;

    }
    public static function _findSets($string, $chrOpen = '(', $includeEnds = true)
    {
        $step = $includeEnds ? 1 : 2;
        $details = $a = [];
        while (true)
        {
            $rv = self::_findSet($string, $chrOpen, $includeEnds, $details);
            if (!$rv) break;

            $a[] = $rv;
            $string = substr($string, $details['setEnd'] + $step);
        }
        return $a;
    }




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  getThisClass
     *
     * Collects and stores various values about 'this' class. The values are stored as class variables and
     * are subsequently used by the document method.
     *
     * @return ReflectionClass
     *
     * @author  Marc Zev
     *
     * @version 2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getThisClass()
    {
        $this->thisClass        = new ReflectionClass($this);
        $this->thisClassMethods = $this->thisClass->getMethods();

//
// ... Refactor array to use method name as index
//
        foreach ($this->thisClassMethods as $idx => $method)
        {
            $methodName = $method->name;
            if ($idx !== $method->getName())
            {
                $this->thisClassMethods[$methodName] = $method;
                unset($this->thisClassMethods[$idx]);
            }


            $mods = array();

//			if ($method->isAbstract()) $mods[] = 'Abstract';
//			if ($method->isConstructor()) $mods[] = 'Constructor';
//			if ($method->isDestructor()) $mods[] = 'Destructor';
            if ($method->isFinal()) $mods[] = 'Final';
            if ($method->isPrivate()) $mods[] = 'Private';
            if ($method->isProtected()) $mods[] = 'Protected';
            if ($method->isPublic()) $mods[] = 'Public';
            if ($method->isStatic()) $mods[] = 'Static';

            $this->thisClassMethodModifiers[$methodName]  = $mods;
            $this->thisClassMethodParameters[$methodName] = $method->getParameters();

            $pars = array();
            foreach ($this->thisClassMethodParameters[$methodName] as $p)
            {
                if (empty($p)) continue;

                $pName = $p->getName();

                $pars = array();

                if ($p->isArray()) $pars[] = 'Array';
                if (method_exists($p, 'isCallable') && $p->isCallable()) $pars[] = 'Callable';
                if ($p->isDefaultValueAvailable()) $pars[] = 'Default_Val_Available';
                if ($p->isOptional()) $pars[] = 'Optional';
                if ($p->isPassedByReference()) $pars[] = 'Passed_by_Ref';

                if (method_exists($p, 'isDefaultValueConstant') && $p->isDefaultValueConstant()) $pars[] = 'Default_Val_Cconstant'; // need PHP 5.4.6 or later
                if (method_exists($p, 'isVariadic') && $p->isVariadic()) $pars[] = 'Variadic';              // needs PHP 5.6 or later

                if (in_array('Default_Val_Available', $pars))
                {
                    $tmp = $p->getDefaultValue();
                    if (is_bool($tmp) && $tmp) $tmp = 'TRUE';
                    if (is_bool($tmp) && !$tmp) $tmp = 'FALSE';

                    if (is_array($tmp) && count($tmp) == 0) $tmp = 'ARRAY()';
                    if (is_array($tmp)) $tmp = 'ARRAY(' . implode(', ', $tmp) . ')';
                    $pars[] = " (Default = " . $tmp . ")";
                }
                $this->thisClassMethodParameters[$methodName][$pName] = $pars;
            }

            $this->thisClassMethodDocBlock[$methodName] = $method->getDocComment();

        }

        return $this->thisClass;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * getThisClassMethod
     *
     * Returns the ReflectionMethod object for the requested method
     *
     * $param string $method Name of class method
     *
     * @return ReflectionMethod|bool Returns FALSE if the requested method does not exist in class
     *
     * @author  Marc Zev
     *
     * @version 2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getThisClassMethod($method)
    {
        if (!empty($this->thisClassMethods) && !isset($this->thisClassMethods[$method])) return false;

        if (empty($this->thisClassMethods)) $this->getThisClass();

        if (!isset($this->thisClassMethods[$method]))
        {
            return false;
        }
        else return $this->thisClassMethods[$method];
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  getThisCode
     *
     * Returns the source code for this class. The source is stored as the class variable $thisCode.
     *
     * $param bool $force If TRUE then the source code is read from disk, regardless of whether it was previously
     * loaded. Default = FALSE.
     *
     * @return string Returns NULL if no source is found.
     *
     * @author  Marc Zev
     *
     * @version 2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getThisCode($force = false)
    {
        if (!isset($this->thisCode) || empty($this->thisCode) || $force)
        {
            if (!isset($this->thisClass) || empty($this->thisClass) || !is_a($this->thisClass, 'ReflectionClass')) return false;

            $this->thisCode = file($this->thisClass->getFileName());

            return $this->thisCode;
        }
        else return $this->thisCode;
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  getThisCodeMethod
     *
     * Returns the source code for the requested method within this class. The source is stored in an element in the
     * class variable array $thisCodeMethods[].
     *
     * $param string $method Name of method
     *
     * @return string Returns NULL if no source is found.
     *
     * @author  Marc Zev
     *
     * @version 2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getThisCodeMethod($method)
    {
        if (isset($this->thisCodeMethods[$method])) return $this->thisCodeMethods[$method];

        $code = $this->getThisCode();

        $store      = false;
        $funcStart  = 0;
        $funcSource = null;
        foreach ($code as $idx => $line)
        {
            if (($lf = stripos($line, 'function')) && ($lm = stripos($line, $method)) && $lf < $lm)
            {
                $startIndex = $idx;
                $search     = substr(implode(null, $code), $funcStart);

                $funcSource = $this->findSet($search, '{', false, $details);
            }
            $funcStart += strlen($line);
        }

        $this->thisCodeMethods[$method] = $funcSource;
        return $funcSource;
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * getVariableDefinition
     *
     * @param string $var
     * @param string $methodSource
     * @param string $format Accepted values are 'string' | 'table'
     *
     * @return string|bool Returns FALSE in case of error
     *
     * @author  Marc Zev
     *
     * @version 2.0.0.2015
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getVariableDefinition($var, $definition, &$methodSource = null, $format = 'table')
    {
        if (empty($var) || empty($methodSource)) return false;

        if (substr($var, 0, 1) != '$') $var = trim('$' . $var);

        $eval = null;
        $loc  = strpos($methodSource, $var);

        if (!$loc) return false;

        $def = $this->findSet(substr($methodSource, $loc), '(', false);

        if (!empty($definition)) $var = $var . " &lt;{$definition}&gt; ";

        switch (strtolower($format))
        {
            case ('string'):
                $eval = $var . ' =  array [ ' . $def . ' ]';
                break;

            case ('table'):
                $eval .= '<table class="shadow">';
                $eval .= "<tr><th colspan='3' style='background-color:#ddd; font-weight:bold;'>{$var}</th></tr>";

                $tuples = explode(',', $def);
                foreach ($tuples as $line)
                {
                    if (strpos($line, '=>'))
                    {
                        list($key, $val) = explode('=>', $line);
                        $eval .= '<tr><td>\'' . trim(str_replace(array('"', '\''), null, $key)) . '\'</td><td>=&gt;</td><td>' . str_replace(array('"', '\''), null, $val) . '</td></tr>';
                    }
                    else $eval .= '<tr><td colspan="3">' . str_replace(array('"', '\''), null, $line) . '</td></tr>';
                }

                $eval .= "</table>\n";
                break;
        }

        return $eval;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  MAGIC METHODS
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __toString
     *
     * Magic Method to render this class as a string
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function __toString()
    {
        if (!isset($this->element)) $this->element = null;
        return (string)$this->render($this->element);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __call
     *
     * Magic Method to call a [non-static] method
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function __call($name, $arguments)
    {
        if (!is_array($arguments)) $arguments = array($arguments);
        $trace = debug_backtrace();
        $drLen = strlen($_SERVER["DOCUMENT_ROOT"]);
        echo "<div id='fatalerror'>" . "\n";
        echo "%% Fatal Error %% <b style='color: firebrick; font-weight: bold;'>Method Not Found</b> %% <b>" . __CLASS__ . "</b>-><b>{$name}</b> (" . implode(', ', $arguments) . ")" . "\n";
        foreach ($trace as $idx => $rec)
        {
            $fName = "..." . substr($rec['file'], $drLen);
            if ($idx == 0) continue;
            echo "<br/>" . str_repeat("&nbsp;", 5) . "called from {$fName} on line {$rec['line']}" . "\n";
        }
        echo "</div>" . "\n";
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __callStatic
     *
     * Magic Method to call a static method
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function __callStatic($name, $arguments)
        /**  As of PHP 5.3.0  */
    {
        if (!is_array($arguments)) $arguments = array($arguments);
        $trace = debug_backtrace();
        $drLen = strlen($_SERVER["DOCUMENT_ROOT"]);
        echo "<div id='fatalerror'>" . "\n";
        echo "%% Fatal Error %% <b style='color: firebrick; font-weight: bold;'>Static Method Not Found</b> %% <b>" . __CLASS__ . "</b>-><b>{$name}</b> (" . implode(', ', $arguments) . ")" . "\n";
        foreach ($trace as $idx => $rec)
        {
            $fName = "..." . substr($rec['file'], $drLen);
            if ($idx == 0) continue;
            echo "<br/>" . str_repeat("&nbsp;", 5) . "called from {$fName} on line {$rec['line']}" . "\n";
        }
        echo "</div>" . "\n";
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __set
     *
     * Magic Method to set a variable
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __get
     *
     * Magic Method to get a variable
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function __get($name)
    {
        if (isset($this->data[$name])) return $this->data[$name];

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __isset
     *
     * Magic Method to check if a class variable is set
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function __isset($name)
        /**  As of PHP 5.1.0  */
    {
        return isset($this->data[$name]);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __unset
     *
     * Magic Method to unset a class variable
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function __unset($name)
        /**  As of PHP 5.1.0  */
    {
        unset($this->data[$name]);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *  __clone
     *
     * Magic Method to clone this class object
     *
     * @author  Marc Zev
     *
     * @version 1.5.1.2012
     */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function __clone()
        /**  As of PHP 5.1.0  */
    {
    }


}


;
