<?php
namespace offer_to_close\common_library_private\Library;

use App\Combine\DocumentCombine;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FormElementHelpers extends Toumai
{

////////////////////////////////////////////////////////////////////////////////////////
/////
/////  buildCheckListFromConstantArray
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function buildCheckListFromConstantArray($fieldName,
                                                           $list = [],
                                                           $display = 'value',
                                                           $store = 'index',
                                                           $default = null)
    {
        if (!$list || !is_array($list) || !count($list)) return (false);
        if (!is_array($default)) $target = [trim($default)];
        $rv = null;

        foreach ($list as $idx => $v)
        {
            $val = trim(($display == 'value') ? $v : $idx);
            $dsp = trim(($store == 'index') ? $idx : $v);
            if (array_search($val, $default) !== false)
            {
                $sel = "CHECKED";
            }
            else $sel = null;
            $rv .= "<input type='checkbox' {$sel} name='{$fieldName}[]' value='{$val}'/>{$dsp}" . "\n";
        }
        return ($rv);
    }


////////////////////////////////////////////////////////////////////////////////////////
/////
/////   buildOptionListFromConstantArray
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function buildOptionListFromConstantArray($default = null,
                                                            $list = [],
                                                            $display = 'value',
                                                            $store = 'index',
                                                            $labelForValue = false,
                                                            $selectAllLabel = false,
                                                            $selectAllValue = false)
    {
        if (!$list || !is_array($list) || !count($list)) return (false);
        if (!$selectAllLabel && !$selectAllValue)
        {
            $rv = null;
        }
        else $rv = "<option value='{$selectAllValue}'>{$selectAllLabel}</option>" . "\n";

        foreach ($list as $idx => $v)
        {
            $val = trim(($display == 'value') ? $v : $idx);
            $dsp = trim(($store == 'index') ? $idx : $v);
            if ($labelForValue) $val = $dsp;

            if (trim($default) == $val)
            {
                $sel = "SELECTED";
            }
            else $sel = null;

            $rv .= "<option {$sel} value='{$val}'>{$dsp}</option>" . "\n";
        }
        return ($rv);
    }
////////////////////////////////////////////////////////////////////////////////////////
/////
/////   buildOptionListFromConstantArray
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function buildOptionListFromArray($default = null,
                                                    $list = [],
                                                    $display = 'Display',
                                                    $store = 'Value',
                                                    $labelForValue = false,
                                                    $selectAllLabel = false,
                                                    $selectAllValue = false)
    {
        if (!$list || !is_array($list) || !count($list)) return ('<option>Storm it</option>' . PHP_EOL);
        if (!$selectAllLabel && !$selectAllValue)
        {
            $optList = [];
        }
        else $optList[] = '<option value="' . $selectAllValue . '">' . $selectAllLabel . '</option>';

        foreach ($list as $idx => $v)
        {
            if (is_a($v, 'Collection'))
            {
                $v = DocumentCombine::collectionToArray($v);
            }

            $val = trim($v[$store] ?? '?');
            $dsp = trim($v[$display] ?? '??');
            if ($labelForValue) $val = $dsp;

            if (trim($default) == $val)
            {
                $sel = 'SELECTED';
            }
            else $sel = null;

            $optList[] = '<option ' . $sel . ' value="' . $val . '">' . $dsp . '</option>';
        }
        return (implode(PHP_EOL, $optList));
    }
////////////////////////////////////////////////////////////////////////////////////////
/////
/////   buildRadioListFromConstantArray
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function buildRadioListFromConstantArray($fieldName,
                                                           $list = [],
                                                           $display = 'value',
                                                           $store = 'index',
                                                           $default = null,
                                                           $suffix = null)
    {
        if (!$list || !is_array($list) || !count($list)) return false;

        $rv = null;

        foreach ($list as $idx => $v)
        {
            $val = trim(($display == 'value') ? $v : $idx);
            $dsp = trim(($store == 'index') ? $idx : $v);
            if ($val == $default)
            {
                $sel = "CHECKED";
            }
            else $sel = null;
            $rv .= "<input type='radio' {$sel} name='{$fieldName}' value='{$val}'/>{$dsp}" . $suffix . "\n";
        }
        return ($rv);
    }

////////////////////////////////////////////////////////////////////////////////////////
/////
/////   buildOptionListFromTable
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function buildOptionListFromTable($default = null,
                                                    $table = null,
                                                    $displayCol = 'value',
                                                    $storeCol = 'index',
                                                    $labelForValue = false,
                                                    $selectAllLabel = false,
                                                    $selectAllValue = false)
    {
        if (!$table || !is_string($table)) return (false);
        if (!$selectAllLabel && !$selectAllValue)
        {
            $rv = null;
        }
        else $rv = "<option value='{$selectAllValue}'>{$selectAllLabel}</option>" . "\n";

        $list = DB::table($table)->get();

        if (!isset($list->first()->{$displayCol}) || !isset($list->first()->{$storeCol}))
        {
            ddd([__METHOD__, __LINE__,$list->first()]);
        }

        foreach ($list as $idx => $v)
        {
            $val = $v->{$displayCol};
            $dsp = trim($v->{$storeCol});

            if ($labelForValue) $val = $dsp;

            if (trim($default) == $val)
            {
                $sel = "SELECTED";
            }
            else $sel = null;

            $rv .= '<option ' . $sel . ' value="' . $val . '">' . $dsp . '</option>' . PHP_EOL;
        }
        return ($rv);
    }
////////////////////////////////////////////////////////////////////////////////////////
/////
/////   buildOptionListFromTable
///
/////  getKeyValuePairListFromTable('lu_UserRoles', ['sortByOrder'=>'a', 'removeSortOrder0=>true,
///  'selectAllLabel'=>'* Select', 'selectAllValue'=>'*',])
////////////////////////////////////////////////////////////////////////////////////////
    public static function getKeyValuePairListFromTable($table = null, $parameters = [])
    {

        $parameterDefaults = ['displayCol'       => 'Display',  // what field display (this is the value in the array)
                              'storeCol'         => 'Value',    // what field to store (this is the index in the array)
                              'labelForValue'    => false,      // true means to store the display field
                              'selectAllLabel'   => false,   // What to display in first row
                              'selectAllValue'   => false,   // what to store if the first row is selected
                              'sortField'        => null,      // "fieldName" or "fieldName|direction" ex: "Name|desc"
                              'sortByOrder'      => false,     // "a" or "d"
                              'removeSortOrder0' => false,    // if true then if order field value = 0 don't display it
                              'titleCaseDisplay' => true,   // convert the display value to title case
        ];

        foreach ($parameterDefaults as $varName => $value)
        {
            $$varName = $parameters[$varName] ?? $value;
        }

        if (!$table || !is_string($table)) return [];

        $retList = [];
        if ($selectAllLabel || $selectAllValue) $retList[$selectAllValue] = $selectAllLabel;

        $query = DB::table($table);

        if ($sortByOrder)
        {
            if (strtolower(substr($sortByOrder,0,1)) == 'd') $direction = 'desc';
            else $direction = 'asc';
            $query = $query->orderBy('Order', $direction);
        }

        if ($sortField)
        {
            @list($field, $direction) = explode('|', $sortField);
            $direction = $direction ?? 'a';
            if (strtolower(substr($direction,0,1)) == 'd') $direction = 'desc';
            else $direction = 'asc';
            $query = $query->orderBy($field, $direction);
        }
        $list = $query->get();

        foreach ($list as $idx => $v)
        {
            if ($removeSortOrder0 && isset($v->Order) && $v->Order <= 0) continue;

            $val = $v->{$displayCol};
            $dsp = trim($v->{$storeCol});

            if ($labelForValue) $val = $dsp;

            $val = $titleCaseDisplay ? title_case( str_replace('_', ' ', $val)) : $val;

            $retList[$val] = $dsp;
        }
        return $retList;
    }


////////////////////////////////////////////////////////////////////////////////////////
/////
/////   buildComplexOptionListFromTable
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function buildComplexOptionListFromTable($table = null, $parameters = [])
    {
        $ddList = [];
        if (!$table || !is_string($table)) return false;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // ... Everything within the displayPattern and storePattern values is interpreted explicitly except when
        //        surrounded by "{{" & "}}". The string within the double curly brackets "{{ }}" are expected to be
        //        valid column names for the provided table.
        //
        //        An example of how to use the pattern is as follows:
        //        'displayPattern' => '{{Lastname}}, {{Firstname}}' would be displayed in the drop-down as
        //        "Zev, Marc"
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $parameterDefaults = ['displayPattern'   => '{ID}', // what to display in dropdown
                              'storePattern'     => '{ID}',   // what value gets stored
                              'default'          => null,   // what to look for to set as default
                              'labelForValue'    => false,  // true means to store the display field
                              'firstRowLabel'    => false,  // what to display in first row - false means no special row
                              'firstRowValue'    => false,  // what to store if the first row is selected
                              'sort'             => null,   // "fieldName" or "fieldName|direction" ex: "Name|desc"
                              'sortByOrder'      => false,  // "a" or "d"
                              'removeSortOrder0' => false,  // if true then if order field value = 0 don't display it
        ];

        // ... Assign variable values based on passed and default parameters
        foreach ($parameterDefaults as $varName => $value)
        {
            $$varName     = $parameters[$varName] ?? $value;
        }

        $columns = array_merge(self::getTokens($displayPattern), self::getTokens($storePattern));
        // ... If firstRow info is provided then use it to define the first row of the drop-down

        if ($firstRowLabel || $firstRowValue) $ddList[] = "<option value='{$firstRowValue}'>{$firstRowLabel}</option>";

        $query = DB::table($table);

        if ($sortByOrder)
        {
            if (strtolower(substr($sortByOrder, 0, 1)) == 'd')
            {
                $direction = 'desc';
            }
            else $direction = 'asc';
            $query = $query->orderBy('Order', $direction);
        }

        if ($sort)
        {
            @list($field, $direction) = explode('|', $sort);
            $direction = $direction ?? 'a';
            if (strtolower(substr($direction, 0, 1)) == 'd')
            {
                $direction = 'desc';
            }
            else $direction = 'asc';
            $query = $query->orderBy($field, $direction);
        }

        $list = $query->get();

        foreach ($columns as $col)
        {
            if (!isset($list->first()->{$col}) || !isset($list->first()->{$col}))
            {
                ddd(['SEND A SCREENSHOT OF THIS PAGE TO MARC',
                    'The "' . $col . '" column was not found',
                    __METHOD__, __LINE__,
                    'displayColumns' => $displayColumns,
                    'storeColumns'   => $storeColumns,
                    'Table'          => $table,
                    '1st record'     => $list->first()]);
            }
        }

        foreach ($list as $idx => $v)
        {
            $val = $storePattern;
            $dsp = $displayPattern;

            foreach ($columns as $col)
            {
                $val = str_replace('{' . $col . '}', $v->{$col}, $val);
                $dsp = str_replace('{' . $col . '}', $v->{$col}, $dsp);
            }

            if ($labelForValue) $val = $dsp;

            if (trim($default) == $val)
            {
                $sel = "SELECTED";
            }
            else $sel = null;

            $ddList[] = '<option ' . $sel . ' value="' . $val . '">' . $dsp . '</option>';
        }
        return implode(PHP_EOL, $ddList);
    }

    public static function getTokens($haystack, $delimiter = '{')
    {
        $i       = 0;
        $details = [];
        $str     = $haystack;
        $rv      = self::_findSet($str, $delimiter, false, $details);

        while ($rv && ++$i < 10)
        {
            $tokens[] = $rv;
            $str      = substr($str, $details['setEnd'] + 2);
            $rv       = self::_findSet($str, $delimiter, false, $details);
        }
        return $tokens;
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    /////
    /////   buildOptionListFromLookupTable
    /////
    ////////////////////////////////////////////////////////////////////////////////////////
    public static function buildOptionListFromLookupTable($default = null,
                                                          $table = null,
                                                          $displayCol = 'Display',
                                                          $storeCol = 'Value',
                                                          $labelForValue = false,
                                                          $selectAllLabel = false,
                                                          $selectAllValue = false,
                                                          $parameters = [])
    {
        $parameterDefaults = [
                              'sortField'        => null,   // "fieldName" or "fieldName|direction" ex: "Name|desc"
                              'sortByOrder'      => false,  // "a" or "d"
                              'removeSortOrder0' => false,  // if true then if order field value = 0 don't display it
                              'titleCaseDisplay' => true,   // convert the display value to title case
        ];

        foreach ($parameterDefaults as $varName => $value)
        {
            $$varName = $parameters[$varName] ?? $value;
        }

        if (!$table || !is_string($table)) return (false);
        if (!$selectAllLabel && !$selectAllValue)
        {
            $rv = null;
        }
        else $rv = '<option value="' . $selectAllValue . '">' . $selectAllLabel . '</option>' . PHP_EOL;

        $query = DB::table($table);

        if ($sortByOrder)
        {
            if (strtolower(substr($sortByOrder,0,1)) == 'd') $direction = 'desc';
            else $direction = 'asc';
            $query = $query->orderBy('Order', $direction);
        }

        if ($removeSortOrder0)
        {
            $query = $query->where('Order', '!=', 0);
        }

        if ($sortField)
        {
            @list($field, $direction) = explode('|', $sortField);
            $direction = $direction ?? 'a';
            if (strtolower(substr($direction,0,1)) == 'd') $direction = 'desc';
            else $direction = 'asc';
            $query = $query->orderBy($field, $direction);
        }

        $list = $query->get();

        foreach ($list as $idx => $v)
        {
            $val = $v->{$storeCol};
            $dsp = trim($v->{$displayCol});

            if ($labelForValue) $val = $dsp;

            if (trim($default) == $val)
            {
                $sel = "SELECTED";
            }
            else $sel = null;

            if ($titleCaseDisplay) $dsp = title_case(str_replace('_', ' ', $dsp));
            $rv .= '<option ' . $sel . ' value="' . $val . '">' . $dsp . '</option>' . PHP_EOL;
        }
        return ($rv);
    }
}