<?php
// offer-to-close\common-library-private\src\CommonLibraryPrivateServiceProvider.php
namespace offer_to_close\common_library_private;

use Illuminate\Support\ServiceProvider;
class CommonLibraryPrivateServiceProvider extends ServiceProvider {
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
/*
 * To distinguish between the default Laravel views and package views, we have to add an extra parameter to our
 * loadviewsfrom() function and that extra parameter should be the name of your package.
 * In our case, it's common_library_private. So now whenever we want to load a view we reference it with this
 * packagename::view syntax convention.
 */
        $this->loadViewsFrom(__DIR__.'/resources/views', 'common_library_private');
    }
    public function register()
    {
    }
}
?>