<?php
namespace offer_to_close\common_library_private\Library;

use \DateInterval;
use \DateTime;
use \Exception;
use Illuminate\Support\Facades\Log;

/*******************************************************************************
 * Class MZ_Time
 *
 * Author: Marc Zev
 * Development Date: Dec 19, 2011
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/
class _Time extends Toumai
{
    public static $dayName = [0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday'];

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// add_date
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function add_date($givendate, $day = 0, $mth = 0, $yr = 0)
    {
        $cd = self::normalizeDate($givendate);

        $newdate = date('Y-m-d h:i:s', mktime(date('h', $cd),
            date('i', $cd), date('s', $cd), date('m', $cd) + $mth,
            date('d', $cd) + $day, date('Y', $cd) + $yr));
        return $newdate;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// addDays ($date = "19800101", $days = 2): $date = date string in any standard date format
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function addDays($date,  $days = 1)
    {
        $theDate = self::normalizeDate($date);

        if (!isset($theDate)) ddd([__METHOD__ => __LINE__, 'date' => $date]);
        $interval = $days * 24 * 60 * 60; // convert days to seconds

        $theDate += $interval;

        return ($theDate);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// addMonths ($date = "19800101", $months = 1): $date = date string in any standard date format
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function addMonths($date, $months = 1)
    {
        $theDate = self::normalizeDate($date);

        $day   = date("j", $theDate);
        $month = date("n", $theDate);
        $year  = date("Y", $theDate);

        if ($months > 12) $months = 12;

        $month += $months;
        if ($month > 12)
        {
            $month = $month - 12;
            $year++;
        }

        $maxDay = date("t", strtotime("$month/1/$year"));
        if ($day > $maxDay) $day = $maxDay;
        $theDate = strtotime("$month/$day/$year");

        return ($theDate);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// addWeeks ($date = "19800101", $weeks = 2): $date = date string in any standard date format
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function addWeeks($date, $weeks = 1)
    {
        $theDate = self::normalizeDate($date);

        $interval = $weeks * 7 * 24 * 60 * 60; // convert weeks to seconds

        $theDate += $interval;

        return ($theDate);
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// addYears ($date = "19800101", $months = 1): $date = date string in any standard date format
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function addYears($date, $years = 1)
    {
        $dateType = false;

        if (is_string($date))
        {
            $theDate  = self::convertToTimestamp($date);
            $dateType = "string";
        }

        if (is_int($date))
        {
            $theDate  = self::convertToTimestamp($date);
            $dateType = "timestamp";
        }

        if ($date instanceof DateTime)
        {
            $theDate  = self::convertToTimestamp($date);
            $dateType = "datetime";
        }

        $day   = date("j", $theDate);
        $month = date("n", $theDate);
        $year  = date("Y", $theDate);
        $year  += $years;

        switch ($dateType)
        {
            case ('string'):
                $theDate = self::convertToDateMDY("{$month}/{$day}/{$year}");
                break;

            case ('timestamp'):
                $theDate = self::convertToTimestamp("{$month}/{$day}/{$year}");
                break;

            case ('datetime'):
                $theDate = self::convertToDateTime("{$month}/{$day}/{$year}");
                break;

            default:
                $theDate = false;
                break;
        }

        return ($theDate);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// countdown($year, $month, $day, $hour, $minute, $format = 0)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function countdown($year, $month, $day, $hour, $minute, $format = 0)
    {
        // make a unix timestamp for the given date
        $the_countdown_date = mktime($hour, $minute, 0, $month, $day, $year, -1);

        // get current unix timestamp
        $today = time();

        $difference = $the_countdown_date - $today;

        //  if ($difference < 0) $difference = 0;
        $days_left    = 0 + floor($difference / 60 / 60 / 24);
        $hours_left   = floor(($difference - $days_left * 60 * 60 * 24) / 60 / 60);
        $minutes_left = floor(($difference - $days_left * 60 * 60 * 24 - $hours_left * 60 * 60) / 60);
        $weeks_left   = round($days_left / 7, 1);
        $years_left   = round($days_left / 365, 2);

        switch ($format)
        {
            case 0: // 0 = Days to go
                if ($years_left > 1)
                {
                    $retval = $years_left . " years to go";
                }
                else
                {
                    if ($weeks_left > 8)
                    {
                        $retval = $weeks_left . " weeks to go";
                    }
                    else
                    {
                        $retval = $days_left . " days to go ";
                    }
                }
                break;
            case 1: // 1 = Today, Target, Difference
                $retval = "Today's date " . date("F j, Y, g:i a") . "<br/>";
                $retval .= "Countdown date " . date("F j, Y, g:i a", $the_countdown_date) . "<br/>";
                $retval .= "Countdown " . $days_left . " days " . $hours_left . " hours " . $minutes_left . " minutes left";
                break;
            case 2: // 2 = Time until Target
                $retval = $days_left . " days " . $hours_left . " hours " . $minutes_left . " minutes ";
                $retval .= "until date " . date("F j, Y, g:i a", $the_countdown_date) . "<br/>";
                echo "i is bar";
                break;
            case 3: //
                $retval = "";
                $retval .= ($days_left > 0) ? $days_left . " days " : "";
                $retval .= ($hours_left > 0) ? $hours_left . " hrs " : "";
                $retval .= ($minutes_left > 0) ? $minutes_left . " min " : "";
                break;
            case 4: //
                $retval = "";
                $retval .= abs($days_left);
                break;
        }
        return ($retval);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// convertToDateMDY ($date): $date to convert
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function convertToDateMDY($date, $delimiter = "/")
    {
        try
        {
            if (is_string($date)) $dt = date("m{$delimiter}d{$delimiter}Y", strtotime($date));
            if (is_int($date)) $dt = date("m{$delimiter}d{$delimiter}Y", $date);
            if ($date instanceof DateTime) $dt = $date->format("m{$delimiter}d{$delimiter}Y");
        }
        catch (Exception $e)
        {
            $dt = false;
        }
        if (!isset($dt)) _Debug::quick($date, "Date");
        if (is_int($dt))
        {
            $format = "m{$delimiter}d{$delimiter}Y";
            $dt     = date($format, $dt);
        }
        return ($dt);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// convertToDateYMD ($date): $date to convert
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function convertToDateYMD($date, $delimiter = '-')
    {
        $dt = false;
        try
        {
            if (is_string($date)) $dt = date("Y{$delimiter}m{$delimiter}d", strtotime($date));
            if (is_int($date)) $dt = date("Y{$delimiter}m{$delimiter}d", $date);
            if ($date instanceof DateTime) $dt = $date->format("Y{$delimiter}m{$delimiter}d");
        }
        catch (Exception $e)
        {
            $dt = false;
        }
        if (!isset($dt)) ddd(['date' => $date, __METHOD__ => __LINE__,]);
        if (is_int($dt))
        {
            $format = "Y{$delimiter}m{$delimiter}d";
            $dt     = date($format, $dt);
        }
        return ($dt);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// convertToDateTime ($date): $date to convert
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function convertToDateTime($date)
    {
        try
        {
            if (is_string($date)) $dt = new DateTime($date);
            if (is_int($date)) $dt = new DateTime(date('Y-m-d H:i:s', $date));
            if ($date instanceof DateTime) $dt = $date;
        }
        catch (Exception $e)
        {
            $dt = false;
        }
        return ($dt);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// convertToDateTimeFromExcel ($date): $date to convert
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function convertToDateTimeFromExcel($excelDate)
    {
        try
        {
            $dt = new DateTime(date('Y-m-d', self::convertToDateTimeFromExcel($excelDate)));
        }
        catch (Exception $e)
        {
            $dt = false;
        }
        return ($dt);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// convertToTimestamp ($date): $date to convert
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function convertToTimestamp($date)
    {
        try
        {
            if (is_string($date)) $ts = strtotime($date);
            if (is_int($date)) $ts = $date;
            if ($date instanceof DateTime) $ts = strtotime($date->format('Y-m-d'));
        }
        catch (Exception $e)
        {
            $ts = false;
        }
        return ($ts);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// convertToTimestampFromExcel ($date): $date to convert
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function convertToTimestampFromExcel($excelDate)
    {
        try
        {
            if ($excelDate <= MIN_DATES_DIFF)
            {
                return 0;
            }

            return ($excelDate - MIN_DATES_DIFF) * SEC_IN_DAY;
        }
        catch (Exception $e)
        {
            return (false);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// daysBetween ($date1 = "19800101", $date2 = "19800101"): $date1 = $date2 as strings in format yyyymmdd
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function daysBetween($date1 = "1/1/1980", $date2 = "1/1/1980")
    {
        // make a unix timestamp for the given date

        // echo $date1 . " db Date 1 = " . substr($date1, 0, 4) . substr($date1, 4, 2) . substr($date1, 6, 2) . "<br>";
        // echo $date2 . " dB Date 2 = " . substr($date2, 0, 4) . substr($date2, 4, 2) . substr($date2, 6, 2) . "<br>";

        $theFirstDate  = (is_string($date1)) ? strtotime($date1) : $date1;
        $theSecondDate = (is_string($date2)) ? strtotime($date2) : $date2;

        $retval = -1;

        if ($theFirstDate && $theSecondDate)
        {
            $retval = $theFirstDate - $theSecondDate;
            $retval = floor($retval / 60 / 60 / 24);
        }

        return ($retval);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// diffDays ($d1, $d2) :
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function diffDays($d1, $d2)
    {
        $datetime1 = self::convertToDateTime($d1);
        $datetime2 = self::convertToDateTime($d2);
        $interval  = $datetime1->diff($datetime2);
        return (round($interval->format('%r%a'), 3));

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// dateDiff ($d1, $d2) :
    ////////
    //////// if $d1 is less than or equal to $d2 then $multiplier is [positive] 1, else is it -1.
    //////// In other words, if multiplier is positive $d1 is less than $d2
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $d1
     * @param $d2
     *
     * @return array The elements of the return array contain the following:
     *               interval: the elements found in the return of PHP diff function
     *               years: integer number of years between dates
     *               months_total: integer number of months between dates, ignoring years
     *               months: integer number of months between dates, after subtracting integer number of years
     *               days_total: integer number of days (rounded up), ignoring years and months
     *               days: integer number of days, after subtracting years and months
     *               weeks: integer number of weeks between dates
     *               hours_total: integer hours between dates (times)
     *               hours: hours (including fractional part) between dates (times)
     *               minutes_total: integer minutes (rounded down) between dates (times)
     *               minutes: integer minutes between dates (times)
     *               seconds_total: seconds, between dates (times)
     *               seconds: integer seconds  between dates (times)
     *               multiplier: -1 if d2 is less than d1, 1 if d2 is greater than d1
     * @throws \Exception
     */
    public static function dateDiff($d1, $d2)
    {
        dump([1, 'd1'=>$d1, 'd2'=>$d2]);

        $d1       = new DateTime(self::convertToDateYMD($d1));
        $d2       = new DateTime(self::convertToDateYMD($d2));
        $interval = $d2->diff($d1);

        $d1 = self::convertToTimestamp($d1);
        $d2 = self::convertToTimestamp($d2);

        if ($d1 <= $d2)
        {
            $multiplier = 1;
        }
        else $multiplier = -1;

        $diff_secs = abs($d1 - $d2);
        $base_year = min(date("Y", $d1), date("Y", $d2));

        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        $rv   = [
            "interval"      => ['y' => $interval->y, 'm' => $interval->m, 'd' => $interval->d,
                                'h' => $interval->h, 'n' => $interval->m, 's' => $interval->s],
            "years"         => date("Y", $diff) - $base_year,
            "months_total"  => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
            "months"        => date("n", $diff) - 1,
            "days_total"    => floor($diff_secs / (3600 * 24)),
            "days"          => date("j", $diff) - 1,
            "weeks"         => floor($diff_secs / (3600 * 24)) / 7,
            "hours_total"   => floor($diff_secs / 3600),
            "hours"         => date("G", $diff),
            "minutes_total" => floor($diff_secs / 60),
            "minutes"       => (int) date("i", $diff),
            "seconds_total" => $diff_secs,
            "seconds"       => (int) date("s", $diff),
            "multiplier"    => $multiplier,
        ];

        return ($rv);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// dateArrayToString ($ay)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function dateArrayToString($ay)
    {
        if (!is_array($ay) || !isset($ay['day']) || !isset($ay['month']) || !isset($ay['year'])) return (false);

        $date = strtotime($ay['month'] . "/" . $ay['day'] . "/" . $ay['year']);
        return ($date);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// dateToArray ($date)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function dateToArray($date)
    {
        return (["month" => date("n", $date), "day" => date("j", $date), "year" => date("Y", $date)]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// DateAdd
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function DateAdd($interval, $number, $date)
    {
        if (is_string($date)) $theDate = strtotime($date);
        if (is_int($date)) $theDate = $date;

        $date_time_array = getdate($theDate);
        $month           = $date_time_array["mon"];
        $day             = $date_time_array["mday"];
        $year            = $date_time_array["year"];

        switch (strtolower($interval))
        {

            case "y":
            case "yyyy":
                $year += $number;
                break;
            case "q":
                $year += ($number * 3);
                break;
            case "m":
                $month += $number;
                break;
            case "d":
                $day += $number;
                if ($day > date("t", strtotime($month . "/1/" . $year)))
                {
                    $day = 1;
                    $month++;
                    if ($month > 12)
                    {
                        $month = 1;
                        $year++;
                    }
                }
                break;
        }
        $timestamp = strtotime("$month/$day/$year");
        return ($timestamp);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// DateSubtract
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function DateSubtract($interval, $number, $date)
    {
        $theDate         = mktime(0, 0, 0, 0 + substr($date, 4, 2), 0 + substr($date, 6, 2), 0 + substr($date, 0, 4), -1);
        $date_time_array = getdate($theDate);
        $hours           = $date_time_array["hours"];
        $minutes         = $date_time_array["minutes"];
        $seconds         = $date_time_array["seconds"];
        $month           = $date_time_array["mon"];
        $day             = $date_time_array["mday"];
        $year            = $date_time_array["year"];

        switch ($interval)
        {

            case "yyyy":
                $year -= $number;
                break;
            case "q":
                $year -= ($number * 3);
                break;
            case "m":
                $month -= $number;
                break;
            case "y":
            case "d":
            case "w":
                $day -= $number;
                break;
            case "ww":
                $day -= ($number * 7);
                break;
            case "h":
                $hours -= $number;
                break;
            case "n":
                $minutes -= $number;
                break;
            case "s":
                $seconds -= $number;
                break;
        }
        $timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
        return self::formatDate($timestamp, 0);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// elapsed time
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function elapsedTime($start, $end)
    {
        $delTime = round($end - $start, 4);

        if ($delTime <= 60)
        {
            $rv = ["hours" => 0, "minutes" => 0, "seconds" => $delTime, "display" => "{$delTime} seconds"];
        }  // Less than a minute
        else
        {
            if ($delTime <= 3600)   // Less than an hour
            {
                $m  = $delTime / 60;
                $s  = round(($m - intval($m)) * 60, 0);
                $m  = intval($m);
                $rv = ["hours" => 0, "minutes" => $m, "seconds" => $s, "display" => "{$m} min, {$s} sec"];
            }
            else // at least an hour
            {
                $h  = $delTime / 3600;
                $m  = ($h - intval($h)) * 60;
                $s  = round(($m - intval($m)) * 60, 0);
                $m  = intval($m);
                $h  = intval($h);
                $rv = ["hours" => $h, "minutes" => $m, "seconds" => $s, "display" => "{$h}:{$m}:{$s}"];
            }
        }
        return ($rv);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// findWeekdayOccurrence ($occurrence, $dayofweek, $month, $year) :
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $occurrence
     * @param $dow   Day of Week as a number, 0 = Sunday, 1 = Monday, etc.
     * @param $month Month of year as a number (1 through 12 for January through December)
     * @param $year  Year (all four digits, e.g. 2018)
     *
     * @return array|null
     */
    public static function findWeekdayOccurrence($occurrence, $dow, $month, $year)
    {
        $dayName = [0 => "Sunday", 1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday"];

        if (is_numeric($dow)) $dow = intval($dow);
        if (is_int($dow))
        {
            $dayofweek = $dayName[$dow];
        }
        else $dayofweek = $dow;

        $dayofweek = _Convert::toTitleCase($dayofweek);

        for ($i = 0; $i < 7; $i++) // Load $days with the first 7 days of the month
        {
            $days[] = date("l", mktime(0, 0, 0, $month, ($i + 1), $year));
        }

        $posd = array_search($dayofweek, $days); // which day of the week is $time's day

        if (is_numeric($occurrence) && $occurrence > 5) $occurrence = "last";

        if (!is_numeric($occurrence))
        {
            switch (strtolower($occurrence))
            {
                case ("first"):
                    $occurrence = 1;
                    break;

                case ("last"):
                    $occ  = 6;
                    $ldom = intval(date("t", mktime(0, 0, 0, $month, 1, $year)));
                    while (($theDate = (7 * $occ) + $posd - 6) > $ldom)
                    {
                        $occ--;
                    }
                    $occurrence = $occ;
                    break;
            }
        }
        $theDate = (7 * $occurrence) + $posd - 6;

        if (checkdate($month, $theDate, $year))
        {
            return ["month" => $month, "day" => $theDate, "year" => $year];
        }
        else return null;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// getFirstSaturday($year=false)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function getFirstSaturday($year = false)
    {
        if (!$year) $year = date('Y');

        $arg = 'First Saturday ' . date('F o', @mktime(0, 0, 0, 1, 1, $year));

        $sat = date('Y-m-d', strtotime($arg));

        return ($sat);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// getWeekEndDate($date)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function getWeekEndDate($date)
    {
        $dow = date('w', strtotime($date));
        if ($dow != 6)
        {
            $dif  = 6 - date("w", strtotime($date));
            $date = date("m/d/Y", _Time::addDays($date, $dif));
        }
        return ($date);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// getWeekOfYear($date, $year=false)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function getWeekOfYear($date, $year = false)
    {
        if (!$year) $year = date('Y', strtotime($date));
        $reference = self::getFirstSaturday($year);

        $difDays = _Time::diffDays($reference, $date);

        $wkNum = ceil($difDays / 7);
        if ($wkNum == 0) ++$wkNum;

        return ($wkNum);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// getWeekdayOccurrence ($time) : returns array where [0] is occurrence and [1] is Day of week (e.g. Friday)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function getWeekdayOccurrence($time)
    {
        $month = intval(date("m", $time));
        $day   = intval(date("d", $time));
        for ($i = 0; $i < 7; $i++) // Load $days with the first 7 days of the month
        {
            $days[] = date("l", mktime(0, 0, 0, $month, ($i + 1), date("Y", $time)));
        }

        $posd  = array_search(date("l", $time), $days); // which day of the week is $time's day
        $posdm = array_search($days[0], $days) - $posd;

        return [(($day + $posdm + 6) / 7), $days[$posd]];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// isWeekday ($origDate) :
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function isWeekday($date)
    {
        try
        {
            $dow = strtolower(date('l', self::convertToTimestamp($date)));

            $retVal = true;
            if ($dow == 'saturday' || $dow == 'sunday') $retVal = false;

            return ($retVal);
        }
        catch (Exception $e)
        {
            ddd(['Exception'=>$e, __METHOD__=>__LINE__,]);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// isHoliday ($date) :
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function isHoliday($date)
    {
        $holidays = [
            ['Holiday' => 'New Year\'s Day', 'State' => '*', 'Type' => 'Absolute', 'Month' => '1', 'Week' => '', 'Day' => '1', 'DOW' => '', 'Adjustment' => '', 'Note' => 'January 1.',],
            ['Holiday' => 'Birthday of Martin Luther King, Jr.', 'State' => '*', 'Type' => 'Relative', 'Month' => '1', 'Week' => '3', 'Day' => '', 'DOW' => 'Monday', 'Adjustment' => '', 'Note' => 'Third Monday in January.',],
            ['Holiday' => 'Presidents\'', 'State' => '*', 'Type' => 'Relative', 'Month' => '2', 'Week' => '3', 'Day' => '', 'DOW' => 'Monday', 'Adjustment' => '', 'Note' => 'Third Monday in February.',],
            ['Holiday' => 'Memorial Day', 'State' => '*', 'Type' => 'Relative', 'Month' => '5', 'Week' => '6', 'Day' => '', 'DOW' => 'Monday', 'Adjustment' => '', 'Note' => 'Last Monday in May.',],
            ['Holiday' => 'Independence Day', 'State' => '*', 'Type' => 'Absolute', 'Month' => '7', 'Week' => '', 'Day' => '4', 'DOW' => '', 'Adjustment' => '', 'Note' => 'July 4.',],
            ['Holiday' => 'Labor Day', 'State' => '*', 'Type' => 'Relative', 'Month' => '9', 'Week' => '1', 'Day' => '', 'DOW' => 'Monday', 'Adjustment' => '', 'Note' => 'First Monday in September.',],
            ['Holiday' => 'Columbus Day', 'State' => '*', 'Type' => 'Relative', 'Month' => '10', 'Week' => '2', 'Day' => '', 'DOW' => 'Monday', 'Adjustment' => '', 'Note' => 'Second Monday in October.',],
            ['Holiday' => 'Veterans Day', 'State' => '*', 'Type' => 'Absolute', 'Month' => '11', 'Week' => '', 'Day' => '11', 'DOW' => '', 'Adjustment' => '', 'Note' => 'November 11.',],
            ['Holiday' => 'Thanksgiving Day', 'State' => '*', 'Type' => 'Relative', 'Month' => '11', 'Week' => '4', 'Day' => '', 'DOW' => 'Thursday', 'Adjustment' => '', 'Note' => 'Fourth Thursday in November.',],
            ['Holiday' => 'Christmas Day', 'State' => '*', 'Type' => 'Absolute', 'Month' => '12', 'Week' => '', 'Day' => '25', 'DOW' => '', 'Adjustment' => '', 'Note' => 'December 25.',],
        ];

        $isHoliday = false;
        foreach ($holidays as $holiday)
        {
            if ($holiday['Type'] == 'Relative')
            {
                $holidate = self::findWeekdayOccurrence($holiday['Week'],
                    array_search($holiday['DOW'], self::$dayName),
                    $holiday['Month'],
                    date('Y', self::convertToTimestamp($date)));
                $holidate = implode('/', $holidate);
            }
            else
            {
                $holidate = date('Y-m-d',
                    strtotime(date('Y', self::convertToTimestamp($date)) . '-' .
                              $holiday['Month'] . '-' . $holiday['Day'])
                );
            }
            if (self::convertToDateYMD($date) == self::convertToDateYMD($holidate)) return true;
        }
        return $isHoliday;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// nextWeekday ($origDate) :
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function nextWeekday($origDate)
    {
        if (strtolower(date('l', $origDate)) == 'friday')
        {
            $newDate = self::add_date($origDate, 3, 0, 0);
        }
        else $newDate = self::add_date($origDate, 1, 0, 0);

        return ($newDate);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// priorWeekday($origDate)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function priorWeekday($origDate)
    {
        if (strtolower(date('l', $origDate)) == 'monday')
        {
            $newDate = self::add_date($origDate, -3, 0, 0);
        }
        else $newDate = self::add_date($origDate, -1, 0, 0);

        return ($newDate);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// addBusinessDays($date, int $dayCount)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function addBusinessDays($date, int $dayCount)
    {
        if ($dayCount > 0)
        {
            for ($i = 0; $i < $dayCount; $i++)
            {
                $date = self::nextWeekday($date);
            }
        }
        else
        {
            if ($dayCount < 0)
            {
                for ($i = 0; $i < $dayCount; $i++)
                {
                    $date = self::priorWeekday($date);
                }
            }
        }
        return $date;
    }

    /**
     * @param $date
     *
     * @return float|int
     */
    public static function moveToBusinessDay($date)
    {
        while (!self::isWeekday($date) || self::isHoliday($date))
        {
            $date = self::addDays($date, 1);
        }
        return $date;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// subtractFromDate ($date = "19800101", $days = 2): $date = date string in any standard date format
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function subtractFromDate($date, $days = 0, $weeks = 0, $months = 0, $years = 0)
    {
        $theDate = self::normalizeDate($date);

        if ($weeks > 0 && $days > 0)     // both days and weeks can't be used at the same time.
        {
            $days  = $days + 7 * $weeks;
            $weeks = 0;
        }

        $interval = "P";
        if ($years > 0) $interval .= $years . "Y";
        if ($months > 0) $interval .= $months . "M";
        if ($weeks > 0) $interval .= $weeks . "W";
        if ($days > 0) $interval .= $days . "D";

        date_sub($theDate, new DateInterval($interval));

        return ($theDate);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////
    //////// formatDate ($date = http://php.net/manual/en/datetime.formats.date.php)
    ////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function formatDate($date, $formatCode = 4)
    {
        $formatCodes = ['Y-m-d', 'Ymd', 'm/d/Y', 'Y-m-d H:i:s', 'n/j/Y', 'fullMonth', 'shortMonth'];
        $theDate = self::normalizeDate($date);

        if (!is_numeric($formatCode)) $formatCode = array_search($formatCode, $formatCodes);

        $formats = ['Y-m-d', 'Ymd', 'm/d/Y', 'Y-m-d H:i:s', 'n/j/Y', 'F j, Y', 'M j, Y',];

        $format = $formats[$formatCode] ?? $formats[4];

        return date($format, $theDate);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// getWordDate
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getWordDate($yr, $mo, $da)
    {
        return (date("F j, Y", mktime(0, 0, 0, $mo, $da, $yr)));
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// NormalizeDate
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static function normalizeDate($date)
    {
        if (is_string($date)) return strtotime($date);
        if (is_int($date)) return $date;
        if ($date instanceof DateTime) return strtotime($date->format('Y-m-d'));

        return $date;
    }

    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
