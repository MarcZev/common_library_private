<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Database parameters for OTC Services family
    |--------------------------------------------------------------------------
    |
    | This array provides the parameters necessary to access the databases for the
    | various OTC services.
    |
    */

    'dbServices' => [
        'otc' => [
            'displayName' => 'Offer To Close',
            'dbConnectionSuffix' => 'otc',
            'isPublic' => true,
        ],

        'oh' => [
            'displayName' => 'OTC Open House',
            'dbConnectionSuffix' => 'oh',
            'isPublic' => true,
        ],

        'admin' => [
            'displayName' => 'OTC Admin',
            'dbConnectionSuffix' => 'admin',
            'isPublic' => false,
        ],

        'offer' => [
            'displayName' => 'OTC Offers',
            'dbConnectionSuffix' => 'offer',
            'isPublic' => true,
        ],

    ],


    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        // **** Offer To Close
        'mysql-otc' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('OTC_DB_DATABASE', 'offer-to-close'),
            'username' => env('OTC_DB_USERNAME', 'root'),
            'password' => env('OTC_DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        // **** OTC Admin

        'mysql-admin' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('ADMIN_DB_DATABASE', 'otcAdmin'),
            'username' => env('ADMIN_DB_USERNAME', 'root'),
            'password' => env('ADMIN_DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        // **** OTC Open House
        'mysql-oh' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('OH_DB_DATABASE', 'openhouse'),
            'username' => env('OH_DB_USERNAME', 'root'),
            'password' => env('OH_DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        // **** OTC Offer
        'mysql-offer' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('OFFER_DB_DATABASE', 'otcOffer'),
            'username' => env('OFFER_DB_USERNAME', 'root'),
            'password' => env('OFFER_DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

    ],

];