<?php
namespace offer_to_close\common_library_private\Library;

use DateTime;
class DisplayTable
{

// ###################################################################################################################
//
//     displayBasic($rows)
//
// ###################################################################################################################
    static public function displayBasic(array $rows, $parameters = array())
    {
        echo self::getBasic($rows,  $parameters);
    }
// ###################################################################################################################
//
//     getBasic($rows)
//
// ###################################################################################################################
    static public function getBasic( $rows, $parameters = array())
    {
        $rows = _Convert::toArray($rows);

        $table = [];
        $paramList = ["zebra-on"           => true,
                      "zebra1"             => "row-zebra-1",
                      "zebra2"             => "transparent",
                      "grid"               => "grid-on",
                      'cellPad' => 'cell-pad',
                      'cellAlign' => 'cell-center',
                      "remove-underscores" => false,
                      "expand-camel"       => false,
                      "showZero"           => false,
                      "showNull"           => false,
                      "showBoolean"        => "check",
                      "showRecordCount"    => false,
                      "removeZeroTime"     => true,
                      "removeZeroDate"     => true,
                      'excludeColumns'     => [],
                      'renameColumns'      => [],
        ];

        $symBool = array('cbxTrue'  => '<span style="font-family: Arial Unicode MS, Lucida Grande">&#9745;</span>',
                         'cbxFalse' => '<span style="font-family: Arial Unicode MS, Lucida Grande">&#9744;</span>',
                         'chkTrue'  => '<span style="font-family: Arial Unicode MS, Lucida Grande">&#10003;</span>',
                         'chkFalse' => '<span style="font-family: Arial Unicode MS, Lucida Grande">&nbsp;</span>',
        );

//        ddd($rows);
        foreach ($paramList as $var => $val)
        {
            if (isset($parameters[$var])) $paramList[$var] = $parameters[$var];
        }
 //               ddd($paramList);
        if (!empty($rows) && is_array($rows) && ($rowCount = count($rows)) > 0)
        {

            $tblRows = array();
            foreach ($rows as $idx => $r)
            {
                if (!is_array($r)) continue;
                $tblRows[$idx] = ($paramList['removeZeroTime']) ? str_replace(['00:00:00.000','00:00:00' ], null, $r) : $r;
                $tblRows[$idx] = ($paramList['removeZeroDate']) ? str_replace(['0000-00-00'], null, $r) : $r;
            }

            if (count($tblRows) == 0)
            {
 //               dump("No Data was provided.");
                return (false);
            }
            $rows = $tblRows;

            $param = [];

            if (!is_array($rows)) ddd($rows, "rows in " . __METHOD__ . ": " . __LINE__, true);

            if (!is_array(reset($rows)))
            {
                $headers = array($rows);
            }
            else $headers = array_keys(reset($rows));

            if ($paramList['showRecordCount'])
            {
                $msgRowCount = "<h2>{$rowCount} records.</h2>" . "\n";
            }
            else $msgRowCount = null;
            $table[] .= $msgRowCount;
            $table[] .= "<table>\n   <tr>" . "\n";

// ... Remove excluded columns
            foreach($headers as $ix=>$txt)
            {
                if (in_array($txt, $paramList['excludeColumns']))
                {
                    unset ($headers[$ix]);
                }
            }

// ... Create Header row

            $gridClass = implode(' ', [$paramList["grid"], $paramList['cellPad'], $paramList['cellAlign']]);

            foreach ($headers as $label)
            {
                if (substr($label, 0, 1) == "_") continue;
                if ($paramList['expand-camel']) $label = _Convert::camelToTitleCase($label);
                if ($paramList['remove-underscores']) $label = str_replace("_", " ", $label);
                $table[] .= "      <td class='{$gridClass}' style='background-color: firebrick; color: white; padding: 2px 5px;'>{$label}</td>" . "\n";
            }
            $table[] .= "   </tr>" . "\n";

            $gridClass = implode(' ', [$paramList["grid"], $paramList['cellPad'], $paramList['cellAlign']]);

// ... Create data rows
            $i = 2;
            foreach ($rows as $idx => $row)
            {
                $i = 3 - $i;
                if ($paramList['zebra-on'])
                {
                    $rowClass = $paramList['zebra' . $i];
                }
                else $rowClass = null;

                $table[] .= "   <tr class='{$rowClass}'>" . "\n";
                foreach ($headers as $idx=>$key)
                {
                    $fld = $row[$key];
                    if (!$paramList['showZero'] && !is_null($fld) && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = null;
                    if (is_bool($fld) && $paramList['showBoolean'] == 'check') $fld = ($fld) ? $symBool['chkTrue'] : $symBool['chkFalse'];
                    if (is_bool($fld) && $paramList['showBoolean'] == 'checkbox') $fld = ($fld) ? $symBool['cbxTrue'] : $symBool['cbxFalse'];
                    if (is_null($fld) && $paramList['showNull']) $fld = 'NULL';
                    if (substr($key, 0, 1) == "_") continue;
                    if ($fld instanceof DateTime)
                    {
                        $table[] .= "<td class='{$gridClass}'>" . $fld->format('Y-m-d H:i:s') . "</td>" . "\n";
                    }
                    else
                    {
                        if (is_object($fld)) $fld = '[' . gettype($fld) . ']';
                        $table[] .= "      <td class='{$gridClass}'>{$fld}</td>" . "\n";
                    }
                }
                $table[] .= "   </tr>" . "\n";
            }
            $table[] .= "</table>" . "\n";
            $table[] .= $msgRowCount;
        }
        else $table[] .= "<h3 style='color:firebrick;'>No Records Found, Table Cannot be Displayed!</h3>" . "\n";
        return implode("\n", $table);
    }
    static public function getTwoLevel( $table, $parameters = array())
    {
        $rv = null;
        $paramList = ['caption-prefix' => 'Level ',
                      ];

        foreach ($paramList as $var => $val)
        {
            if (isset($parameters[$var])) $paramList[$var] = $parameters[$var];
        }

        try
        {
            foreach ($table as $caption => $section)
            {
                $rv .= '<h1>' . $paramList['caption-prefix'] . ' ' . $caption . '</h1>' . PHP_EOL;
                $rv .= self::getBasic($section, $parameters) . PHP_EOL . PHP_EOL;
            }
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION'=>$e,'table'=>$table]);
        }
        return $rv;
    }
}
