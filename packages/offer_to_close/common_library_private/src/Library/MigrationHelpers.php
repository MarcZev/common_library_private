<?php
namespace offer_to_close\common_library_private\Library;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationHelpers
{

////////////////////////////////////////////////////////////////////////////////////////
/////
/////  Assign Standard Address Fields
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function assignStandardAddressFields(Blueprint &$table)
    {
        $table->string('Street1')->nullable();
        $table->string('Street2')->nullable();
        $table->string('Unit')->nullable();
        $table->string('City')->nullable();
        $table->string('State')->nullable();
        $table->string('Zip')->nullable();
    }


////////////////////////////////////////////////////////////////////////////////////////
/////
/////  Assign Standard Name Fields
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function assignStandardNameFields(Blueprint &$table)
    {
        $table->string('NameFull')->nullable();
        $table->string('NameFirst')->nullable();
        $table->string('NameLast')->nullable();
    }


////////////////////////////////////////////////////////////////////////////////////////
/////
/////  Assign Standard Name Fields
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function assignStandardContactFields(Blueprint &$table)
    {
        $table->string('PrimaryPhone')->nullable();
        $table->string('SecondaryPhone')->nullable();
        $table->string('Email')->nullable()->comment='If they have login the default is the username.';
        $table->string('EmailFormat')->default('html')->comment='Should the emails be in text or html formats';
        $table->string('ContactSMS')->nullable()->comment='If a valid phone number then send texts to it.';

    }

////////////////////////////////////////////////////////////////////////////////////////
/////
/////  Assign Standard Swah Fields - SWAH => Stuff We All Have
/////
////////////////////////////////////////////////////////////////////////////////////////
    public static function assignStandardSwahFields(Blueprint &$table)
    {
        $table->boolean('isTest')->default(0)->comment='Standard table field used for testing';
        $table->string('Status')->nullable();
        $table->string('Notes')->nullable();
        $table->timestamp('DateCreated')->nullable();
        $table->timestamp('DateUpdated')->nullable();
        $table->softDeletes();

    }

}