<?php

namespace offer_to_close\common_library_private\Library;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use \RecursiveIteratorIterator;
use \RecursiveDirectoryIterator;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;


/*******************************************************************************
 * Class _Log
 *
 * Author: Marc Zev
 * Development Date: Dec 19, 2011
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/
class _Log extends Toumai
{
    /**
     * @return array
     */
    public static function IAmHere($justFile = true)
    {
        $facade = 'Illuminate\\Support\\Facades\\Facade';
        $thisFile = pathinfo(__FILE__, PATHINFO_BASENAME);

        $trace = self::stackDump();
        $loc   = reset($trace);
        while (pathinfo($loc['file'], PATHINFO_BASENAME) == $thisFile)
        {
            $loc = next($trace);
        }

        $line  = (isset($loc['line'])) ? $loc['line'] : null;
        $file  = (isset($loc['file'])) ? pathinfo($loc['file'], PATHINFO_BASENAME) : null;
        $class = (isset($loc['class'])) ? $loc['class'] : null;
        $type = (isset($loc['type'])) ? $loc['type'] : null;
        $function = (isset($loc['function'])) ? $loc['function'] : null;

        if ($justFile) $place = $file . ': ' . $line;
        else
        {
            if ($class != $facade)
            {
                $place = '(' . $file . ') ' . $class . $type . $function . ': ' . $line;
            }
            else $place = $file . ': ' . $line;
        }
        return ($place);
    }


    public static function stackDump($trimToApp = true)
    {
        $outputKeys = ['id', 'sourceType', 'currFile', 'function', 'line', 'file', 'class', 'object', 'type', 'args'];
        $thisFile = pathinfo(__FILE__, PATHINFO_BASENAME);
        $output = [];

        $trace = debug_backtrace();
        $appFound = false;
        foreach($trace as $idx=>$loc)
        {
            if (isset($loc['file']) && stripos($loc['file'], 'vendor')) $prefix['sourceType'] = 'system';
            elseif (isset($loc['class']) && stripos($loc['class'], 'Kernel')) $prefix['sourceType'] = 'system';
            else $prefix['sourceType'] = 'app';

            if (isset($loc['file']) && pathinfo($loc['file'], PATHINFO_BASENAME) == $thisFile) $prefix['currFile'] = '*';
            else  $prefix['currFile'] = ' ';

            $prefix['id'] = $idx;

            if ($trimToApp)
            {
                if (!$appFound) $output[] = array_merge(array_fill_keys($outputKeys, null), $prefix, $loc);
                else if ($prefix['sourceType'] == 'app') $output[] = array_merge(array_fill_keys($outputKeys, null), $prefix, $loc);

                $appFound = ($appFound || $prefix['sourceType'] == 'app');
            }
            else $output[] = array_merge(array_fill_keys($outputKeys, null), $prefix, $loc);
        }
        return ($output);
    }
    public static function formatStackDump($trimToApp = true)
    {
        $stack = self::stackDump($trimToApp);

        $output[] = self::IAmHere();

        foreach($stack as $level)
        {
            $l = [];
            $l[] = $level['id'];
            if (isset($level['file'])) $l[] = 'File: ' . pathinfo($level['file'], PATHINFO_BASENAME) .' (' .
            $level['line'] .')';
            if (isset($level['class'])) $l[] = 'Class: ' . $level['class'] . $level['type']. $level['function'];
            $l[] = $level['currFile'];
            $output[] = implode('|', $l);
        }
        return ($output);
    }

    public static function logIt($logMsg='test', $logName='otc', $logLevel='info', $byDate=true)
    {
        ddd(['i am here'=>self::IAmHere()]);
        $logPath = 'logs/' .
                   $byDate ? date('Y-m-d/') : null .
                   $logName . strpos($logName, '.') ? null : '.log' ;
        $orderLog = new Logger($logName);
        $orderLog->pushHandler(new StreamHandler(storage_path($logPath)), $logLevel);
        $orderLog->info($logName, $logMsg);
    }
}
