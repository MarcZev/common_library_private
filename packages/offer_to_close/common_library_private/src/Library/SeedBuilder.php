<?php
namespace offer_to_close\common_library_private\Library;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SeedBuilder extends Toumai
{

    ////////////////////////////////////////////////////////////////////////////////////////
    /////
    /////  buildClass
    /////
    ////////////////////////////////////////////////////////////////////////////////////////
    public function buildClass($tableName, $className=null, $classDir = null)
    {
        $fh    = false;
        if (empty($className)) $className = '_Add';
        $className = $className . '_' . $tableName . '_' . date('Ymd');

        $classDir   = empty($classDir) ? database_path('seeds/') : $classDir;
        $file       = $classDir . $className . '.php';
        if (!is_dir($classDir)) ddd(['no such directory' => $classDir, 'real path' => realpath($classDir),]);
        $tableCount = DB::table($tableName)->count(); //get();
        $data       = [];

        $limStart = 1;
        $limEnd   = 5000 ;

        if ($tableCount < $limEnd) $cycles = 1;
        else $cycles = ceil((float) $tableCount / (float) $limEnd);

        $fh = fopen($file, 'w');
        fwrite($fh, $this->classTop($className) . PHP_EOL);

        for ($cyc = 1; $cyc <= $cycles; $cyc++)
        {
                        $lStart = $limStart + ($cyc - 1) * $limEnd;

            $table = DB::table($tableName)->take($limEnd)->skip($lStart)->get();

            foreach ($table as $idx => $rec)
            {
                $ayRec      = (array) $rec;
                $data[$idx] = explode("\n", var_export($ayRec, true));
                unset($data[$idx][count($data[$idx]) - 1]);
                unset($data[$idx][0]);
            }

            try
            {
                fwrite($fh, PHP_EOL. '    $stuff = [' . PHP_EOL);
                $code = [];
                foreach ($data as $idx => $rec)
                {
                    $line = '[' . implode('', $rec) . '],';
                    if (!fwrite($fh, $line . PHP_EOL)) ddd([__METHOD__, __LINE__, 'ERROR'=>'Failed to write '. $line,]);
                    $code[] = $line;
                }
                fwrite($fh, '                ];

        DB::table(\'' . $tableName . '\')->create($stuff);'.PHP_EOL.PHP_EOL.PHP_EOL);
            }
            catch (\Exception $e)
            {
                if ($fh) fclose($fh);
                ddd(['file' => $file, 'real path' => realpath($file), 'exception' => $e,]);
            }
//            if ($cyc > 10) break;
        }
        fwrite($fh, $this->classBottom());
        fclose($fh);
        return $file;

    }

    private function classTop($name)
    {
        return '
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class '. $name .' extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    ';
    }

    private function classBottom()
    {
        return '
    }
}
';
    }
}