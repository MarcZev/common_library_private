<?php
namespace offer_to_close\common_library_private\Library;
/** *****************************************************************************
 * Class DisplayTools
 *
 * Author: Marc Zev
 * Development Date: Dec 19, 2014
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/

class DisplayTools extends Toumai
{
    /**
    // ###################################################################################################################
    //
    //     editTables($rows)

    //   @document paramList, symBool
    @document pCode
    // ###################################################################################################################
     */
    static public function editTable($rows, $parameters=array(), $returnString=false)
    {
        $table = null;

        $paramList = ["zebra-on"           => true,
                      "zebra1"             => "row-zebra-1",
                      "zebra2"             => "transparent",
                      "grid"               => "grid-on",
                      "remove-underscores" => false,
                      "showZero"           => false,
                      "showBoolean"        => "check",
                      "showRecordCount"    => false,
                      "showEditButton"     => true,
                      "buttonLabel"        => 'Edit',
                      "qs"                 => [],
                      'lookupFields'       => [],
                      'lookupColumns'      => [],
                      'primaryKey'         => null,
                      'removeZeroTime'     => true,
                      'removeZeroDate'     => true,
                      "returnString"       => true,
                      'excludeColumns'     => [],
                      'renameColumns'      => [],
                      'booleanColumns'     => [],
                      "expandCamel"        => true,
                      "titleCase"          => true,
                      "*method"            => null];

        $symBool = array('cbxTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9745;</span>',
                         'cbxFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9744;</span>',
                         'chkTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#10003;</span>',
                         'chkFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&nbsp;</span>',
        );

        foreach($paramList as $var=>$val)
        {
            if (isset($parameters[$var])) $paramList[$var] = $parameters[$var];
        }

        if (!empty($rows) && is_array($rows) && ($rowCount = count($rows)) > 0)
        {
            $tblRows = [];
            foreach ($rows as $idx=>$r)
            {
                if (!is_array($r)) continue;  // Skip non-array rows
                // ... Remove Zero Times and Zero Dates
                foreach ($r as $i=>$v )
                {
                    $tblRows[$idx][$i] = ($paramList['removeZeroTime']) ? str_replace(['00:00:00.000','00:00:00',], null, $v) : $v;
                    $v = $tblRows[$idx][$i];
                    $tblRows[$idx][$i] = ($paramList['removeZeroDate']) ? str_replace(['0000-00-00',], null, $v) : $v;
                }
            }

            if (count($tblRows) == 0)
            {
                return (false);
            }
            $rows = $tblRows;

            if (!is_array(reset($rows))) $headers = array($rows);
            else $headers = array_keys(reset($rows));

            foreach($headers as $ix=>$txt)
            {
                if (array_search($txt, $paramList['excludeColumns']) !== false)
                {
                    unset ($headers[$ix]);
                    continue; // skip the columns that should be excluded
                }
                if (isset($paramList['lookupFields'][$txt]))
                {
                    $t = $txt;
                    $t = str_replace('ies_ID', 'y', $t);
                    //					$t = str_replace('es_ID', 'e', $t);
                    $t = str_replace('es_ID', null, $t);
                    $t = str_replace('s_ID', null, $t);
                    $t = str_replace('_ID', null, $t);
                    $headers[$ix] = $t;
                }
            }

            if ($paramList['showRecordCount']) $msgRowCount =  "<h2>{$rowCount} records.</h2>" ."\n";
            else $msgRowCount = null;
            $table .=  $msgRowCount;
            $table .=  "<table class='otc-edit-table'>\n   <tr>" ."\n";

            $renameColumns = count($paramList['renameColumns']);

            foreach($headers as $label)
            {
                if (substr($label,0,1) == "_") continue;
                // ... Rename Columns ...
                if ($renameColumns && isset($paramList['renameColumns'][$label])) $label = $paramList['renameColumns'][$label];
                if ($paramList['remove-underscores']) $label = str_replace("_", " ", $label);
                if ($paramList['expandCamel']) $label = _Convert::camelToTitleCase($label);
                if ($paramList['titleCase']) $label = title_case($label);
                $table .=  "      <td style='background-color: DodgerBlue; color: white; padding: 2px 5px;'>{$label}</td>" ."\n";
            }
            if ($paramList['showEditButton']) $table .=  "      <td style='background-color: DodgerBlue; color: white; padding: 2px 5px;'>&nbsp;</td>" ."\n";
            $table .=  "   </tr>" ."\n";

            $gridClass = $paramList["grid"];

            $i = 2;
            foreach($rows as $idx=>$row)
            {
                $i = 3 - $i;
                if ($paramList['zebra-on']) $rowClass = $paramList['zebra'.$i];
                else $rowClass = null;

                $table .=  "   <tr class='{$rowClass}'>" ."\n";
                foreach($row as $key=>$fld)
                {
                    if (array_search($key, $paramList['excludeColumns']) !== false) continue; // skip the columns that should be excluded
                    if (isset($paramList['lookupFields'][$key]))
                    {
                        $lu = $paramList['lookupFields'][$key];
                        if (isset($lu[$fld]))
                        {
                            $c = $paramList['lookupColumns'][$key];
                            if (strpos($c, '+') !== false)
                            {
                                list($c, $pk) = explode("+", $c);
                            }
                            $fld = $lu[$fld][$c];
                        }
                        $t = str_replace('es_ID', null, $t);
                        $t = str_replace('ies_ID', 'y', $t);
                        $t = str_replace('s_ID', null, $t);
                        $t = str_replace('_ID', null, $t);
                        $headers[$ix] = $t;
                    }

                    if (isset($paramList['primaryKey'])) $pk = $paramList['primaryKey'];
                    else $pk = 'ID';
                    $btnParameters = array('action'=>'edit', 'recIndex'=>$row[$pk]);
                    if (!empty($paramList['*method'])) $btnParameters['*method'] = $paramList['*method'];

                    if (count($paramList['qs']) > 0)
                    {
                        foreach($paramList['qs'] as $vr=>$vl)
                        {
                            $btnParameters[$vr] = $vl;
                        }
                    }
                    $btnRow = FormElements::standardButton($paramList['buttonLabel'], $btnParameters);

                    if (!$paramList['showZero'] && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = null;
                    if (is_bool($fld) || in_array($key, $paramList['booleanColumns']))
                    {
                        if ($paramList['showBoolean'] == 'check') $fld = ($fld) ? $symBool['chkTrue'] : $symBool['chkFalse'];
                        if ($paramList['showBoolean'] == 'checkbox') $fld = ($fld) ? $symBool['cbxTrue'] : $symBool['cbxFalse'];
                    }
                    if (substr($key,0,1) == "_") continue;
                    if ($fld instanceof \DateTime)
                    {
                        $table .=  "<td class='{$gridClass}'>".$fld->format('Y-m-d H:i:s')."</td>" . "\n";
                    }
                    else $table .=  "      <td class='{$gridClass}'>{$fld}</td>" ."\n";
                }
                if ($paramList['showEditButton']) $table .=  "      <td class='{$gridClass}'>{$btnRow}</td>" ."\n";
                $table .=  "   </tr>" ."\n";
            }
            $table .=  "</table>" . "\n";
            $table .=  $msgRowCount;
        }
        else $table .=   "<h3 style='color:firebrick;'>No Records Found, Table Cannot be Displayed!</h3>". "\n";

        if ($paramList['returnString']) return ($table);
        else echo $table;
    }


    /**
    // ###################################################################################################################
    //
    //     editChildTable($rows, $showDashboardButton, $parmeters)
    //
    // ###################################################################################################################
     */
    static public function editChildTable($rows, $parameters=array())
    {
        $paramList = ["zebra-on"           => true,
                      "zebra1"             => "row-zebra-1",
                      "zebra2"             => "transparent",
                      "grid"               => "grid-on",
                      "remove-underscores" => false,
                      "showZero"           => false,
                      "showBoolean"        => "check",
                      "showRecordCount"    => false,
                      "buttonLabel"        => 'Edit',
                      "qs"                 => [],
                      'lookupFields'       => [],
                      'lookupColumns'      => [],
                      'primaryKey'         => null,
                      "removeZeroTime"     => true,
                      'parent'             => null,
                      'parentPK'           => 'ID',
                      'parentFields'       => [],
                      'foreignKey'         => null,
                      'foreignKeyValue'    => null,
        ];

        $symBool = array('cbxTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9745;</span>',
                         'cbxFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9744;</span>',
                         'chkTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#10003;</span>',
                         'chkFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&nbsp;</span>',
        );

        // ... Set default parameters as needed
        foreach($paramList as $var=>$val)
        {
            if (isset($parameters[$var])) $paramList[$var] = $parameters[$var];
        }

        // ... Deal with getting and displaying the Parent 'Table' data
        if (!empty($paramList['parent']))
        {
            $tblParent = $paramList['parent'];
            $parentPK = $paramList['parentPK'];
            $parentFields = $paramList['parentFields'];
            $foreignKey = $paramList['foreignKey'];
            $foreignKeyValue = $paramList['foreignKeyValue'];

            if (!is_array($parentFields)) $parentFields = array($parentFields=>$parentFields);
            $kys = array_keys($parentFields);
            $flds = array_values($parentFields);

            if (empty($parentFields)) $fields = "*";
            else
            {
                $fields = null;
                foreach ($kys as $idx=>$k)
                {
                    $f = $flds[$idx];
                    $fs[]  = "{$k} as {$f}";
                }
                $fields = implode (", ", $fs);
            }

            // ... If parent is a table name then create a select out of it
            if (stripos($parent, 'select') === false)
            {
                $parent = "SELECT {$fields} FROM {$tblParent} ";
            }

            if (!empty($paramList['parentPK']) && !empty($paramList['foreignKeyValue']))
            {
                $parent .= "WHERE {$paramList['parentPK']} = '{$paramList['foreignKeyValue']}'";
            }

            $rows = array();
            $result = _DB::executeQuery($parent);
            if (!$result)
            {
                ddd("Kalak's Breath! There was an error running this query.");
                ddd($parent, "Parent Table Select", true);
            }
            if (sqlsrv_has_rows($result) == 0)
            {
                ddd("Storm it! There is no matching parent record in `{$tblParent}` for {$paramList['parentPK']} = {$paramList['foreignKeyValue']}.");
            }
            else
            {
                while ($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC))
                {
                    $pRows[] = $r;
                }
                self::table($pRows, false, $parameters);
            }
        }

        // ... Process and display rows
        if (!empty($rows) && is_array($rows) && ($rowCount = count($rows)) > 0)
        {
            $tblRows = array();
            foreach ($rows as $idx=>$r)
            {
                if (!is_array($r)) continue;
                $tblRows[$idx] = ($paramList['removeZeroTime']) ? str_replace(" 00:00:00.000", null, $r) : $r;
                $tblRows[$idx] = $r;
            }

            if (count($tblRows) == 0)
            {
                ddd("Data provided for table is unusable.");
                return (false);
            }

            $rows = $tblRows;

            // ... if there are no rows - report it.
            if (!is_array($rows)) ddd($rows, " rows in ".__METHOD__. ": ".__LINE__, true);

            // ... Define headers for header row, and clean up the labels
            if (!is_array(reset($rows))) $headers = array($rows);
            else $headers = array_keys(reset($rows));

            foreach($headers as $ix=>$txt)
            {
                if (isset($paramList['lookupFields'][$txt]))
                {
                    $t = $txt;
                    $t = str_replace('ies_ID', 'y', $t);
                    $t = str_replace('es_ID', null, $t);
                    $t = str_replace('s_ID', null, $t);
                    $t = str_replace('_ID', null, $t);
                    $headers[$ix] = $t;
                }
            }

            // ...
            if ($paramList['showRecordCount']) $msgRowCount =  "<h2>{$rowCount} records.</h2>" ."\n";
            else $msgRowCount = null;
            echo $msgRowCount;
            echo "<table>\n   <tr>" ."\n";

            // ... Display header row
            foreach($headers as $label)
            {
                if (substr($label,0,1) == "_") continue;
                if ($paramList['remove-underscores']) $label = str_replace("_", " ", $label);
                echo "      <td style='background-color: DodgerBlue; color: white; padding: 2px 5px;'>{$label}</td>" ."\n";
            }
            echo "      <td style='background-color: DodgerBlue; color: white; padding: 2px 5px;'>&nbsp;</td>" ."\n";
            echo "   </tr>" ."\n";

            $gridClass = $paramList["grid"];

            // ... Process and display each data row
            $i = 2;
            foreach($rows as $idx=>$row)
            {
                $i = 3 - $i;
                if ($paramList['zebra-on']) $rowClass = $paramList['zebra'.$i];
                else $rowClass = null;

                echo "   <tr class='{$rowClass}'>" ."\n";

                // ... Process and display each row value
                foreach($row as $key=>$fld)
                {
                    // .. If the current column has a field with a defined lookup table, then use it to display the desired value.
                    if (isset($paramList['lookupFields'][$key]))
                    {
                        $lu = $paramList['lookupFields'][$key];
                        if (isset($lu[$fld]))
                        {
                            $c = $paramList['lookupColumns'][$key];
                            if (strpos($c, '+') !== false)
                            {
                                list($c, $pk) = explode("+", $c);
                            }
                            $fld = $lu[$fld][$c];
                        }
                        $t = str_replace('es_ID', null, $t);
                        $t = str_replace('ies_ID', 'y', $t);
                        $t = str_replace('s_ID', null, $t);
                        $t = str_replace('_ID', null, $t);
                        // ... Make sure the colunn header displays the desired value
                        $headers[$ix] = $t;
                    }

                    // ... Figure out the primary key for the current record to pass to the edit button.
                    if (isset($paramList['primaryKey'])) $pk = $paramList['primaryKey'];
                    else $pk = 'ID';
                    //'parent'=>null, 'parentPK'=>'ID', 'foreignKey'=>null, 'foreignKeyValue'=>null
                    $btnParameters = array('action'=>'edit', 'recIndex'=>$row[$pk]);
/*
                            $tblParent = $paramList['parent'];
                            $parentPK = $paramList['parentPK'];
                            $foreignKey = $paramList['foreignKey'];
                            $foreignKeyValue = $paramList['foreignKeyValue'];
*/

                    $btnParameters = array_merge($btnParameters, array('parent'=>$tblParent??null,
                                                                       'parentPK'=>$parentPK??null,
                                                                       'foreignKey'=>$foreignKey??null,
                                                                       'foreignKeyValue'=>$row[$row['foreignKey']??null]??null));
                    if (count($paramList['qs']) > 0)
                    {
                        foreach($paramList['qs'] as $vr=>$vl)
                        {
                            $btnParameters[$vr] = $vl;
                        }
                    }

                    // Create row edit button
                    $btnRow = FormElements::standardButton($paramList['buttonLabel'], $btnParameters);

                    if (!$paramList['showZero'] && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = null;
                    if (is_bool($fld) && $paramList['showBoolean'] == 'check') $fld = ($fld) ? $symBool['chkTrue'] : $symBool['chkFalse'];
                    if (is_bool($fld) && $paramList['showBoolean'] == 'checkbox') $fld = ($fld) ? $symBool['cbxTrue'] : $symBool['cbxFalse'];
                    if (substr($key,0,1) == "_") continue;
                    if ($fld instanceof \DateTime)
                    {
                        echo "<td class='{$gridClass}'>".$fld->format('Y-m-d H:i:s')."</td>" . "\n";
                    }
                    else echo "      <td class='{$gridClass}'>{$fld}</td>" ."\n";
                }
                echo "      <td class='{$gridClass}'>{$btnRow}</td>" ."\n";
                echo "   </tr>" ."\n";
            }
            echo "</table>" . "\n";
            echo $msgRowCount;
        }
        else echo "<h3 style='color:firebrick;'>No Records Found, Table Cannot be Displayed!</h3>". "\n";
    }

    /**
    // ###################################################################################################################
    //
    //     linkTable($rows)
    //
    // ###################################################################################################################
     */
    static public function linkTable($rows,$showDashboardButton=false,$parameters=array())
    {
        $paramList = array("zebra-on"=>true, "zebra1"=>"row-zebra-1", "zebra2"=>"transparent",
                           "grid"=>"grid-on", "remove-underscores"=>false, "showZero"=>false,
                           "showBoolean"=>"check", "showRecordCountTop"=>false, "showRecordCountBottom"=>false, "removeZeroTime"=>true,
                           "headerRepeat"=>0, "yesNoFields"=>array("isEnabled"), "trueFalseFields"=>array(),
                           "linkColumn"=>array(), "link"=>_Get::SV('href').'?pc='._Get::sv('pCode'), 'linkAdd'=>'&var=' );

        $symBool = array('cbxTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9745;</span>',
                         'cbxFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9744;</span>',
                         'chkTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#10003;</span>',
                         'chkFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&nbsp;</span>',
        );

        $table = null;

        $yesNo = array("No", "Yes");
        $trueFalse = array ("False", "True");

        foreach($paramList as $var=>$val)
        {
            if (!isset($parameters[$var])) $parameters[$var] = $val;
        }

        $paramList = $parameters;

        if (!is_array($paramList['linkColumn'])) $paramList['linkColumn'] = array($paramList['linkColumn']);

        if (!empty($rows) && is_array($rows) && ($rowCount = count($rows)) > 0)
        {

            $tblRows = array();
            foreach ($rows as $idx=>$r)
            {
                if (!is_array($r)) continue;
                $tblRows[$idx] = ($paramList['removeZeroTime']) ? str_replace(" 00:00:00.000", null, $r) : $r;
            }

            if (count($tblRows) == 0)
            {
                _Print::notice("Data provided for table is unusable.");
                return (false);
            }
            $rows = $tblRows;

            $parm = array('pc'=>'dashboard');
            if ($showDashboardButton) $table .= "<div class='no-print'>" . FormElements::linkButton('Return to Dashboard', $parm) . "</div>" . "\n";

            if (!is_array($rows)) _Print::c($rows, "rows in ".__METHOD__. ": ".__LINE__, true);

            if (!is_array(reset($rows))) $headers = array($rows);
            else $headers = array_keys(reset($rows));

            if ($paramList['showRecordCountTop']) _Print::info("{$rowCount} records.");
            $table .=  "<table>\n   <tr>" ."\n";

            $headerString = null;
            foreach($headers as $label)
            {
                if (substr($label,0,1) == "_") continue;
                if ($paramList['remove-underscores']) $label = str_replace("_", " ", $label);
                $headerString .= "      <td style='background-color: firebrick; color: white; padding: 2px 5px;'>{$label}</td>" ."\n";
            }
            $headerString .= "   </tr>" ."\n";
            $table .=  $headerString;

            $gridClass = $paramList["grid"];

            $i = 2;
            $hdrCount = 0;
            foreach($rows as $idx=>$row)
            {
                ++$hdrCount;

                if ($paramList["headerRepeat"] > 0 && ($paramList["headerRepeat"] % $hdrCount) == 0)
                {
                    $i = 2;
                    $hdrCount = 0;
                    $table .=  $headerString;
                }
                $i = 3 - $i;
                if ($paramList['zebra-on']) $rowClass = $paramList['zebra'.$i];
                else $rowClass = null;

                $table .=  "   <tr class='{$rowClass}'>" ."\n";
                foreach($row as $key=>$fld)
                {
                    if (array_search($key, $paramList['yesNoFields']) !== false)
                    {
                        $tFld = (is_null($fld) || empty($fld)) ? 0 : $fld;
                        $tFld = ($tFld !== 0 && $tFld && $tFld != 1 ? 1 : $tFld);
                        $fld =  $yesNo[$tFld];
                    }
                    if (array_search($key, $paramList['trueFalseFields']) !== false)
                    {
                        $tFld = (is_null($fld) || empty($fld)) ? 0 : $fld;
                        $tFld = ($tFld !== 0 && $tFld && $tFld != 1 ? 1 : $tFld);
                        $fld =  $trueFalse[$tFld];
                    }

                    if (array_search($key, $paramList['linkColumn']) !== false)
                    {
                        $lnk =  $paramList['link'].$paramList['linkAdd'].$fld;
                        $tFld  = "<a href='";
                        $tFld .= (is_null($fld) || empty($fld)) ? null : $lnk ;
                        $tFld .= "'>{$fld}</a>";
                        $fld = $tFld;
                    }
                    if (!$paramList['showZero'] && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = null;
                    if (is_bool($fld) && $paramList['showBoolean'] == 'check') $fld = ($fld) ? $symBool['chkTrue'] : $symBool['chkFalse'];
                    if (is_bool($fld) && $paramList['showBoolean'] == 'checkbox') $fld = ($fld) ? $symBool['cbxTrue'] : $symBool['cbxFalse'];
                    if ($paramList['showZero'] && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = "0";

                    if (substr($key,0,1) == "_") continue;
                    if ($fld instanceof DateTime)
                    {
                        $table .=  "<td class='{$gridClass}'>".$fld->format('Y-m-d H:i:s')."</td>" . "\n";
                    }
                    else $table .=   "      <td class='{$gridClass}'>{$fld}</td>" ."\n";
                }
                $table .=  "   </tr>" ."\n";
            }
            $table .=  "</table>" . "\n";

            if ($paramList['showRecordCountBottom']) ddd("{$rowCount} records.");

        }
        else $table .=  "<h3 style='color:firebrick;'>No Records Found, Table Cannot be Displayed!</h3>". "\n";

        return ($table);
    }


    /**
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ... monthOptList
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
     */
    public static function monthOptList($target=null, $order='asc')
    {
        if (strtolower(substr($order, 0, 1) == 'a')) $isDesc = false;
        elseif (strtolower(substr($order, 0, 1) == 'd')) $isDesc = true;

        $rv = null;
        for ($i=1; $i<=12; ++$i)
        {
            if ($isDesc) $j = 13 - $i;
            else $j = $i;

            if (trim($target) == $j) $sel = "SELECTED";
            else $sel = null;

            $n = date('M', strtotime("{$j}/1/2000"));
            $rv .= "<option {$sel} value='{$j}'>{$n}</option>"."\n";
        }

        return ($rv);
    }
    /**
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ... numberOptList
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
     */
    public static function numberOptList($min=1, $max=12, $target=null, $order='asc')
    {
        if ($max < $min)
        {
            $t = $min;
            $min = $max;
            $max = $t;
        }

        if (strtolower(substr($order, 0, 1) == 'a')) $isDesc = false;
        elseif (strtolower(substr($order, 0, 1) == 'd')) $isDesc = true;

        $rv = null;
        for ($i=$min; $i<=$max; ++$i)
        {
            if ($isDesc) $j = ($min + $max) - $i;
            else $j = $i;

            if (trim($target) == $j) $sel = "SELECTED";
            else $sel = null;

            $rv .= "<option {$sel} value='{$j}'>{$j}</option>"."\n";
        }

        return ($rv);
    }


    // ###################################################################################################################
    //
    //     table($rows)
    //
    // ###################################################################################################################
    static public function table($rows,$showDashboardButton=false,$parameters=array())
    {
        $paramList = array("zebra-on"=>true, "zebra1"=>"row-zebra-1", "zebra2"=>"transparent",
                           "grid"=>"grid-on", "remove-underscores"=>false, "showZero"=>false,
                           "showBoolean"=>"check", "showRecordCountTop"=>false, "showRecordCountBottom"=>false, "removeZeroTime"=>true,
                           "headerRepeat"=>0, "yesNoFields"=>array("isEnabled"), "trueFalseFields"=>array());

        $symBool = array('cbxTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9745;</span>',
                         'cbxFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#9744;</span>',
                         'chkTrue'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&#10003;</span>',
                         'chkFalse'=>'<span style="font-family: Arial Unicode MS, Lucida Grande">&nbsp;</span>',
        );

        $table = null;

        $yesNo = array("No", "Yes");
        $trueFalse = array ("False", "True");

        foreach($paramList as $var=>$val)
        {
            if (!isset($parameters[$var])) $parameters[$var] = $val;
        }

        $paramList = $parameters;

        if (!empty($rows) && is_array($rows) && ($rowCount = count($rows)) > 0)
        {

            $tblRows = array();
            foreach ($rows as $idx=>$r)
            {
                if (!is_array($r)) continue;
                $tblRows[$idx] = ($paramList['removeZeroTime']) ? str_replace(" 00:00:00.000", null, $r) : $r;
            }

            if (count($tblRows) == 0)
            {
                ddd("Data provided for table is unusable.");
                return (false);
            }
            $rows = $tblRows;

            $parm = array('pc'=>'dashboard');
            if ($showDashboardButton) $table .= "<div class='no-print'>" . FormElements::linkButton('Return to Dashboard', $parm) . "</div>" . "\n";

            if (!is_array($rows)) ddd($rows, "rows in ".__METHOD__. ": ".__LINE__, true);

            if (!is_array(reset($rows))) $headers = array($rows);
            else $headers = array_keys(reset($rows));

            if ($paramList['showRecordCountTop']) ddd("{$rowCount} records.");
            $table .=  "<table>\n   <tr>" ."\n";

            $headerString = null;
            foreach($headers as $label)
            {
                if (substr($label,0,1) == "_") continue;
                if ($paramList['remove-underscores']) $label = str_replace("_", " ", $label);
                $headerString .= "      <td style='background-color: firebrick; color: white; padding: 2px 5px;'>{$label}</td>" ."\n";
            }
            $headerString .= "   </tr>" ."\n";
            $table .=  $headerString;

            $gridClass = $paramList["grid"];

            $i = 2;
            $hdrCount = 0;
            foreach($rows as $idx=>$row)
            {
                ++$hdrCount;

                if ($paramList["headerRepeat"] > 0 && ($paramList["headerRepeat"] % $hdrCount) == 0)
                {
                    $i = 2;
                    $hdrCount = 0;
                    $table .=  $headerString;
                }
                $i = 3 - $i;
                if ($paramList['zebra-on']) $rowClass = $paramList['zebra'.$i];
                else $rowClass = null;

                $table .=  "   <tr class='{$rowClass}'>" ."\n";
                foreach($row as $key=>$fld)
                {
                    if (array_search($key, $paramList['yesNoFields']) !== false)
                    {
                        $tFld = (is_null($fld) || empty($fld)) ? 0 : $fld;
                        $tFld = ($tFld !== 0 && $tFld && $tFld != 1 ? 1 : $tFld);
                        $fld =  $yesNo[$tFld];
                    }
                    if (array_search($key, $paramList['trueFalseFields']) !== false)
                    {
                        $tFld = (is_null($fld) || empty($fld)) ? 0 : $fld;
                        $tFld = ($tFld !== 0 && $tFld && $tFld != 1 ? 1 : $tFld);
                        $fld =  $trueFalse[$tFld];
                    }

                    if (!$paramList['showZero'] && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = null;
                    if (is_bool($fld) && $paramList['showBoolean'] == 'check') $fld = ($fld) ? $symBool['chkTrue'] : $symBool['chkFalse'];
                    if (is_bool($fld) && $paramList['showBoolean'] == 'checkbox') $fld = ($fld) ? $symBool['cbxTrue'] : $symBool['cbxFalse'];
                    if ($paramList['showZero'] && !is_bool($fld) && is_numeric($fld) && $fld == 0) $fld = "0";

                    if (substr($key,0,1) == "_") continue;
                    if ($fld instanceof \DateTime)
                    {
                        $table .=  "<td class='{$gridClass}'>".$fld->format('Y-m-d H:i:s')."</td>" . "\n";
                    }
                    else $table .=   "      <td class='{$gridClass}'>{$fld}</td>" ."\n";
                }
                $table .=  "   </tr>" ."\n";
            }
            $table .=  "</table>" . "\n";

            if ($paramList['showRecordCountBottom']) ddd("{$rowCount} records.");

        }
        else $table .=  "<h3 style='color:firebrick;'>No Records Found, Table Cannot be Displayed!</h3>". "\n";

        return ($table);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ... processDataModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function processDataModel($dataModelFile, $tableSelect)
    {
        if (empty($tableSelect) || !is_file($dataModelFile) || empty($dataModelFile)) return (array());


        $lookupTables = _Files::readIniFile($dataModelFile, true);

        list($db, $schema, $table) = explode(".", $tableSelect);
        $pk = (isset($primaryKeys[$tableSelect])) ? $primaryKeys[$tableSelect] : null;

        if ($schema == 'array')

            if (isset($lookupTables[$tableSelect]))
            {
                foreach ($lookupTables[$tableSelect] as $col=>$luTable)
                {
                    if (strtolower($col) == '_primary-key')
                    {
                        $pk = $luTable;
                        continue;
                    }

                    $columns = false;
                    list($dbTmp, $schemaTmp, $tTmp) = explode(".", $luTable);
                    if (strpos($tTmp, "|") !== false) list($tableTmp,$columns) = explode("|", $tTmp);
                    else $tableTmp  = $tTmp;

                    $tblTmp = new Generic_Table($tableTmp, $dbTmp, $schemaTmp);
                    $lookups[$col] = $tblTmp->getAllRecords(null, true);
                    $cols[$col] = $columns;
                }
                unset($tblTmp);
            }

        $tab = $table;
        if (substr($tab, 0, 2) == 'lu') $tab = substr($tab, 2);
        else if (substr($tab, 0, 2) == 'op') $tab = substr($tab, 2);

        $parameters = array('qs'=>array('tableSelect'=>$tableSelect), 'lookupFields'=>$lookups, 'lookupColumns'=>$cols, 'primaryKey'=>$pk);

        return ($parameters);
    }
}
