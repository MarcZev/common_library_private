<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 1/31/2019
 * Time: 1:04 PM
 */

namespace offer_to_close\common_library_private\Library;


class _Crypt
{
    public static function base64url_encode( $data ){
        return str_replace('%3D','=',urlencode(base64_encode($data)));
    }

    public static function base64url_decode( $data ){
        return str_replace('=','%3D',base64_decode(urldecode($data)));
    }
}