<?php

namespace offer_to_close\common_library_private\Library;


/*******************************************************************************
 * Class _Variables
 *
 * Author: Marc Zev
 * Development Date: Aug 8, 2018
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/

class _Variables
{
    //////////////////////////////////////////////////////////////////////////////////////////
    /////
    ///// function displayAsTable($array, $useKey=false, $class=null, $id=null, $fieldList=array())
    /////
    //////////////////////////////////////////////////////////////////////////////////////////
    public static function getObjectName($obj, $getShortName=true)
    {
        if (!is_object($obj)) return false;
        try
        {
            if ($getShortName) return (new \ReflectionClass($obj))->getShortName();
            return get_class($obj);
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    /**
     * @param $string
     *
     * @return bool TRUE if the string is valid JSON, false otherwise
     */
    public static function isJson($string)
    {
        if (!is_string($string)) return false;
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}