<?php

namespace offer_to_close\common_library_private\Library;

Use DateTime;
use Illuminate\Support\Facades\Config;
/*******************************************************************************
 * Class Debug
 *
 * Author: Marc Zev
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/


class _Debug
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function clarify ($var)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function clarify($var)
    {
        if ($var instanceof DateTime)
        {
            $tmp = $var;
            $var = (string)$tmp->format('Y-m-d H:i:s') . " (DT)";
        }
        if (is_bool($var))
        {
            if ($var === true) $var = (string)'`TRUE`';
            if ($var === false) $var = (string)'`FALSE`';
        }
        if ($var == null || strlen($var) == 0) $var = "``";

        return ($var);
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function c ($str, $func="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @deprecated
     *
     * @param        $var
     * @param string $func
     * @param bool   $die
     * @param bool   $debug
     */
    static public function c($var, $func = "", $die = false, $debug = true)
    {
        self::comment($var, $func, $die, $debug);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function comment ($str, $func="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function comment($var, $func = "", $die = false, $debug = true)
    {
        $level = 0;
        $place = " <span style='color:#aaa;'>(from " . self::IAmHere() . ")</span>";

        $func .= $place;
        if (self::logIt())
        {
            _Files::writeToLog($var, $func);
        }
        else
        {
            if ($debug) print ("\n<debug>\n");
            print ("\n<!-- -------| $func |------------------ printComment -->\n");
            print ("<div class='print-comment'>\n   <pre>");
            if (is_array($var))
            {
                print ('<span style="color:blue font-weight:bold;">Dump Array: ' . $func . "</span><br>\n");
            }
            else if (!empty($func)) print('<span style="color:black; font-weight:bold;"> ' . $func . "</span><br>\n");
            echo '   <br><span style="color:green; font-weight:bold;"> <pre>';

            if (is_array($var))
            {
                self::displayArray($var, "[-ARRAY-]", $level);
            }
            else print_r($var);

            echo "   </pre></span><br>\n";
            echo "</div> <!-- end: .print-comment --> \n";
            if ($die) echo "Processing stopped due to method argument.\n";
            if ($debug) echo"\n</debug>\n";
        }
        if ($die) die;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static private function displayArray ($ay, $label)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static private function displayArray($ay, $label, $level = 0)
    {
        ksort($ay);
        echo "{$label} [array] => " . "<br/>" . "\n";
        foreach ($ay as $key => $val)
        {
            if (is_array($val))
            {
                self::displayArray($val, $key, $level + 1);
            }
            else echo str_repeat("\t", $level) . "`{$key}` => " . self::clarify($val) . "<br/>" . "\n";
        }
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function error($str, $func="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function error($str, $func = "", $parameters = true)
    {
// - - - - - -
        $def = array('debug' => true, 'print' => true, 'screen' => true);
        if (!is_array($parameters)) $parameters = array('debug' => $parameters);
        foreach ($def as $key => $val)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $val;
        }
        foreach ($parameters as $var => $val)
        {
            $$var = $val;
        }
        $classes = "";
        if (isset($print) && $print == false) $classes .= " no-print";
        if (isset($screen) && $screen == false) $classes .= " just-print";
// - - - - - -


        if (self::logIt())
        {
            _Files::writeToLog($var, $func);
        }
        else
        {
            print ("\n<!-- -------| $func |------------------- printError -->\n");
            print ("<table border=1 class= '{$classes}' bgcolor=white><tr><td>\n");
            if (!empty($func)) print ('<b><span style="color:black"> ' . $func . "</span></b><br>\n");
            print ('<br><b><span style="color:red"> ' . $str . '</span></b>' . "<br>\n");
            print ("</td></tr></table>\n");
        }
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function hidden($str, $func="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function hidden($var, $func = "", $debug = false)
    {
        if ($debug) print ("\n<debug>\n");
        if (self::logIt())
        {
            _Files::writeToLog($var, $func);
        }
        else
        {
            if (is_array($var))
            {
                print ("<!-- --------------------------------------------------------------- dumpArrayHidden -----------\n");
            }
            else print ("\n<!-- -------| $func |---------------- printHidden ----- -->\n");

            print_r("<!--     \n");
            if (is_array($var))
            {
                self::displayArray($var, "[-ARRAY-]");
            }
            else print_r($var);
            print_r("\n -->\n");

            print ("<!-------------------------------------------- -->\n");
        }
        if ($debug) print ("\n</debug>\n");
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function q ($str, $func="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @deprecated
     *
     * @param        $str
     * @param string $func
     * @param bool   $die
     * @param bool   $debug
     */
    static public function q($str, $func = "", $die = false, $debug = true)
    {
        self::quick($str, $func, $die, $debug);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function quick ($str, $func="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function quick($str, $func = null, $die = false, $debug = true)
    {
        $place = " <span style='color:#aaa;'>(from " . self::IAmHere() . ")</span>";

//	$func .= " from {$place}";
        if (!is_null($func) && !is_bool($func) && is_numeric($func) && $func == 0) $func = '`0`';
        if (!is_null($str) && !is_bool($str) && is_numeric($str) && $str == 0) $str = '`0`';
        if (!empty($func) && substr(trim($func), -1) != ":") $func .= ": ";
        if (self::logIt())
        {
            _Files::writeToLog($str, $func);
        }
        else
        {
            echo "<div class='print-quick'>";
            if (is_array($str))
            {
                if ($debug) print ("\n<debug>\n");
                print ("\n<!-- - - - - - - -| $func |------------(Array)---- printQuick -->\n");
                if (!empty($func)) print ('<b><span style="color:black;">' . $func . "</span>&nbsp;</b>\n");
                self::displayArray($str, "[-ARRAY-]");
                if ($debug) print ("\n</debug>\n");
            }
            else
            {
                if ($debug) print ("\n<debug>\n");
                print ("\n<!-- -------| $func |------------(Array)---- printQuick -->\n");
                if (!empty($func)) print ('<b><span style="color:black">' . self::clarify($func) . "</span>&nbsp;\n");

                print ('<span style="color:green"> ' . self::clarify($str) . "</span></b> {$place} <br/>\n");
                if ($die) print ("Processing stopped due to method argument.\n");
                if ($debug) print ("\n</debug>\n");
            }
            echo "</div>\n";
        }
        if ($die) die();
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function sectionEnd ($str)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * This is a quick method to make the underlying HTML easier to read. This method is paired with sectionStart ($str).
     */
    static public function sectionEnd($str)
    {
        $ttl = "+++++++| $str |+++++++";
        $c   = strlen($ttl);

        echo "\n<!-- {$ttl} -->";
        echo "\n<!-- " . str_repeat("^", $c) . " -->\n\n";
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function sectionStart ($str)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * This is a quick method to make the underlying HTML easier to read. This method is paired with sectionEnd ($str).
     */
    static public function sectionStart($str)
    {
        $ttl = "+++++++| $str |+++++++";
        $c   = strlen($ttl);

        echo "\n\n<!-- " . str_repeat("v", $c) . " -->";
        echo "\n<!-- {$ttl} -->\n\n";
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  whereAmI ()
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function whereAmI($debug = false)
    {
        $place = self::IAmHere();

        if (self::logIt())
        {
            _Files::writeToLog($place);
        }
        else
        {
            if ($debug) print ("\n<debug>\n");
            print ("\n<!-- -------| $place |---------------- WhereAmI -->\n");
            echo "<p class='whereami'>" . $place . "</p>" . "\n";
            if ($debug) print ("\n</debug>\n");
        }
        return;
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  iAmHere ()
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function IAmHere()
    {
        $thisFile = pathinfo(__FILE__, PATHINFO_BASENAME);

        $trace = debug_backtrace();
        $loc   = reset($trace);
        while (pathinfo($loc['file'], PATHINFO_BASENAME) == $thisFile)
        {
            $loc = next($trace);
        }

        $line  = (isset($loc['line'])) ? $loc['line'] : null;
        $file  = (isset($loc['file'])) ? pathinfo($loc['file'], PATHINFO_BASENAME) : null;
        $place = "{$file}: {$line}";

        return ($place);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  notice($str)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function notice($str, $parameters = true, $debug = false)
    {
// - - - - - -
        $def = array('debug' => true, 'print' => true, 'screen' => true);
        if (!is_array($parameters)) $parameters = array('debug' => $parameters);
        foreach ($def as $key => $val)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $val;
        }
        foreach ($parameters as $var => $val)
        {
            $$var = $val;
        }
        $classes = "print-notice";
        if (isset($print) && $print == false) $classes .= " no-print";
        if (isset($screen) && $screen == false) $classes .= " just-print";
// - - - - - -
        $trace = debug_backtrace();
        $loc   = reset($trace);
        $line  = (isset($loc['line'])) ? $loc['line'] : null;
        $file  = (isset($loc['file'])) ? pathinfo($loc['file'], PATHINFO_BASENAME) : null;
        $place = "{$file}: {$line}";

        if ($debug) print ("\n<debug>\n");
        print ("\n<!-- -------| $place |---------------- printNotice -->\n");
        print ("<p class='{$classes}'>");
        print (self::clarify($str));
        print ("</p>");
        if ($debug) print ("\n</debug>\n");
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  info($str)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function info($str, $parameters = true, $debug = false)
    {
        $def = array('debug' => true, 'print' => true, 'screen' => true);

        if (!is_array($parameters)) $parameters = array('debug' => $parameters);

        foreach ($def as $key => $val)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $val;
        }

        foreach ($parameters as $var => $val)
        {
            $$var = $val;
        }

        $classes = "print-info";
        if (isset($print) && $print == false) $classes .= " no-print";
        if (isset($screen) && $screen == false) $classes .= " just-print";


        $trace = debug_backtrace();
        $loc   = reset($trace);
        $line  = (isset($loc['line'])) ? $loc['line'] : null;
        $file  = (isset($loc['file'])) ? pathinfo($loc['file'], PATHINFO_BASENAME) : null;
        $place = "{$file}: {$line}";

        if ($debug) print ("\n<debug>\n");
        print ("\n<!-- -------| $place |---------------- printInfo -->\n");
        print ("<p class='{$classes}'>");
        print (self::clarify($str));
        print ("</p>");
        if ($debug) print ("\n</debug>\n");
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  alert($str)
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function alert($str, $stop = false, $parameters = true, $debug = false)
    {
// - - - - - -
        $def = array('debug' => true, 'print' => true, 'screen' => true);
        if (!is_array($parameters)) $parameters = array('debug' => $parameters);
        foreach ($def as $key => $val)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $val;
        }
        foreach ($parameters as $var => $val)
        {
            $$var = $val;
        }
        $classes = "print-alert";
        if (isset($print) && $print == false) $classes .= " no-print";
        if (isset($screen) && $screen == false) $classes .= " just-print";
// - - - - - -

        $trace = debug_backtrace();
        $loc   = reset($trace);
        $line  = (isset($loc['line'])) ? $loc['line'] : null;
        $file  = (isset($loc['file'])) ? pathinfo($loc['file'], PATHINFO_BASENAME) : null;
        $place = "{$file}: {$line}";

        if ($debug) print ("\n<debug>\n");
        print ("\n<!-- -------| $place |---------------- printNotice -->\n");
        print ("<p class='{$classes}'>");
        print (self::clarify($str));
        print ("</p>");
        if ($stop) print ("Processing stopped by request.\n");
        if ($debug) print ("\n</debug>\n");
        if ($stop) die(date("** g:i:s a **"));
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  superGlobals ()
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function superGlobals()
    {
        if (isset($_REQUEST))
        {
            self::c($_REQUEST, "_REQUEST");
        }
        else self::q("_REQEST not found");

        if (isset($_FILE))
        {
            self::c($_FILE, "_FILE");
        }
        else self::q("_FILE not found");

        if (isset($_SESSION))
        {
            self::c($_SESSION, "_SESSION");
        }
        else self::q("_SESSION not found");

        if (isset($_SERVER))
        {
            self::c($_SERVER, "_SERVER");
        }
        else self::q("_SERVER not found");

        return;
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////  static public function tag($str, $label="")
/////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function tag($var, $label = "", $debug = false)
    {
        if ($debug) print ("\n<debug>\n");
        if (self::logIt())
        {
            _Files::writeToLog($var, $label);
        }
        else
        {
            if (is_array($var))
            {
                $st = null;
                foreach ($var as $ky => $vr)
                {
                    $st .= "{$ky}=>{$vr}, ";
                }
                print ("\n\n<!-- TAG: # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #  {$label}: {$st} -->\n\n");
            }
            else print ("\n\n<!-- TAG:  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #  {$label}: {$var} -->\n\n");

        }
        if ($debug) print ("\n</debug>\n");
    }

/////////////////////////////////////////////////////////////////////////////////////////
/////
///// function logIt()
/////
//////////////////////////////////////////////////////////////////////////////////////////
    /**
     *
     */
    static public function logIt()
    {
        return _Get::logFlag();
    }


//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function openLogFile($str)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function openLogFile()
    {
        $fh = @fopen(Config::get('constants.LOG_FILE'), "w")
        or die ("Can't open file (and your little dog too.");

        date_default_timezone_set('America/Los_Angeles');
        fputs($fh, "Log created at " . date('l jS \of F Y h:i:s A'));
        fputs($fh, "\r\n");
        fputs($fh, "\r\n");

        fclose($fh);
    }


}


