<?php

namespace offer_to_close;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;


class ConfigManagement
{
    public static function postUpdate(Event $event)
    {
        $homeDir = $event->getComposer()->getConfig()->get('home');

        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        dd(['homeDir'=>$homeDir, 'vendorDir'=>$vendorDir]);

        $files = glob($vendorDir . '/*Module/config/*.php');

        foreach ($files as $file) {
            copy($file, $homeDir.'/config/offer_to_close_'.basename($file));
        }
    }

}