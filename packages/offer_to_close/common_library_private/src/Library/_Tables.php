<?php

namespace offer_to_close\common_library_private\Library;

/*******************************************************************************
 * Class _Tables
 *
 * Author: Marc Zev
 * Development Date: Aug 10, 2018
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/
class _Tables
{

    public static function emptyTable($tableName, $resetIndex=true)
    {
        if (empty($tableName)) return false;

        $rv  = DB::table($tableName)->delete();

        if ($resetIndex)
        {
            $max = DB::table($tableName)->max('id') + 1;
            $rv = DB::statement('ALTER TABLE ' . $tableName . ' AUTO_INCREMENT = ' . $max);
        }
        return $rv;
    }
}