<?php

namespace offer_to_close\common_library_private\Library;

use App\Library\otc\Credentials;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/*******************************************************************************
 * Class _Convert
 *
 * Author: Marc Zev
 * Development Date: Jun 6, 2018
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/
class _Convert
{
    //////////////////////////////////////////////////////////////////////////////////////////
    /////
    ///// function displayAsTable($array, $useKey=false, $class=null, $id=null, $fieldList=array())
    /////
    //////////////////////////////////////////////////////////////////////////////////////////
    public static function collectionToArray(Collection $collection)
    {
        $ay = [];
        foreach ($collection as $idx => $record)
        {
            foreach ($record as $key => $val)
            {
                $ay[$idx][$key] = $val;
            }
        }
        return $ay;
    }

    public static function stdClassToArray($stdClass)
    {
        if (!is_a($stdClass, 'StdClass')) return $stdClass;

        $ay = [];
        foreach ($stdClass as $idx => $value)
        {
            $ay[$idx] = self::stdClassToArray($value);
        }
        return $ay;
    }

    public static function toArray($source)
    {

        if (is_a($source, 'StdClass')) return self::stdClassToArray($source);
        if (_Variables::getObjectName($source) == 'Collection')
        {
            return $source->toArray();
        }
        if (is_a($source, 'object')) return self::stdClassToArray($source);
        if (is_array($source))
        {
            $top = reset($source);
            if (is_array($top))
            {
                return $source;
            }
            else if (is_a($top, 'StdClass') || _Variables::getObjectName($source) == 'Collection')
            {
                foreach ($source as $idx=>$row)
                {
                    $source[$idx] = self::toArray($row);
                }
                return $source;
            }
            else return $source;
        }

//        if (isServerLocal()) Log::info([__CLASS__ . '::' . __FUNCTION__ . '(' . __LINE__ . ')',
//                   'Data Type of source' => gettype($source)]);

        return $source;
    }

    static public function toTitleCase($str = null, $specialCapWords = null)
    {
        $sr = trim(ucwords(strtolower($str)));

        if (!empty($specialCapWords))
        {
            foreach ($specialCapWords as $v)
            {
                $sv = ' ' . $v . ' ';
                $sr = ' ' . $sr . ' ';
                $sr = str_ireplace($sv, strtoupper($sv), $sr);
            }
        }
        return trim($sr);
    }

    static public function splitCamel($string)
    {
        $string = preg_replace('/(?<=\\w)(?=[A-Z])/', ' $1', $string);
        return trim($string);
    }

    static public function camelToTitleCase($str = null, $specialCapWords = null)
    {
        $str = self::splitCamel($str);
        $sr  = trim(ucwords(strtolower($str)));

        if (!empty($specialCapWords))
        {
            foreach ($specialCapWords as $v)
            {
                $sv = ' ' . $v . ' ';
                $sr = ' ' . $sr . ' ';
                $sr = str_ireplace($sv, strtoupper($sv), $sr);
            }
        }
        return trim($sr);
    }

    public static function formatDollars($amount, $decimals = 0)
    {
        if (!is_numeric($amount)) return $amount;

        return '$' . number_format($amount, $decimals);
    }
}