<?php

namespace offer_to_close\common_library_private\Library;


/*******************************************************************************
 * Class _Files
 *
 * Author: Marc Zev
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/

class _Files
{

//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    isImage ($file)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
    static public function isImage($file)
    {
        $types = array('gif', 'jpg', 'png', 'tif');
        $ext   = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        $rv    = in_array($ext, $types);
        return ($rv);
    }


//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    isPDF ($file)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
    static public function isPDF($file)
    {
        $types = array('pdf');
        $ext   = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        $rv    = in_array($ext, $types);
        return ($rv);
    }



//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    placeEmpty ($file)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
    static public function placeEmpty($file)
    {
        $fh = fopen($file, 'w');
        fclose($fh);
        return;
    }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    readArrayFromCSVFile ($file, $withHeader)
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function readArrayFromCSVFile($file, $skipFirstLine = false, $trimColumns = true, $headerLabels=false)
    {
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "1200M"); // This routine can be a memory hog, so we bump it up while running then reduce it at the end.

        $lineCnt = 0;
        $records = array();

        if (!is_file($file))
        {
            trigger_error("File: $file not found. ", E_USER_ERROR);
            exit(3);
        }

        if (is_file($file))
        {
            $idx   = 0;
            $lines = file($file, FILE_SKIP_EMPTY_LINES);

            if (!$skipFirstLine)
            {
                $header = str_getcsv(array_shift($lines), ",", '"');
                $header = array_map('trim', $header);
                if ($headerLabels)
                {
                    foreach($header as $idx=>$hdr)
                    {
                        if (isset($headerLabels[$idx])) $header[$idx] = $headerLabels[$idx];
                    }
                }
            }

            $i = 0;
            foreach ($lines as $idx => $rr)
            {
                $row = str_getcsv($rr, ",", '"');
                if ($trimColumns)
                {
                    $len = min(count($header), count($row));
                    $tr  = array_slice($row, 0, $len);
                    $th  = array_slice($header, 0, $len);
                }
                else
                {
                    $tr = $row;
                    $th = $header;
                }


                if (!$skipFirstLine)
                {
                    $indexedRow = array_combine($th, $tr);
                }
                else $indexedRow = $tr;
                $data[] = $indexedRow;
                unset($lines[$idx]);
                ++$idx;
            }

            ini_set("memory_limit", $initMem);
            return ($data);
        }

        ini_set("memory_limit", $initMem);
        return (false);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    readCSVFromFile ($file)
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function readCSVFromFile($file)
    {
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "1200M"); // This routine can be a memory hog, so we bump it up while running then reduce it at the end.

        $lineCnt = 0;
        $records = array();

        if (!is_file($file))
        {
            trigger_error("File: $file not found. ", E_USER_ERROR);
            ini_set("memory_limit", $initMem);
            return (false);
        }

        $lines = file($file, FILE_SKIP_EMPTY_LINES);

        ini_set("memory_limit", $initMem);
        return ($lines);

    }

//////////////////////////////////////////////////////////////////////////////////////////
/////
///// readIniFile ($iniFile, $sectionFlag=false)
/////
//////////////////////////////////////////////////////////////////////////////////////////

    static public function readIniFile($iniFile, $sectionFlag = false)
    {
        if (is_array($iniFile))
        {
            $a = true;
        }


        if (empty($iniFile)) return (false);
        if (!is_file($iniFile)) return (false);

        $sysVars = parse_ini_file($iniFile, $sectionFlag);

        return ($sysVars);
    }



//////////////////////////////////////////////////////////////////////////////////////////
/////
///// readFileToArray ($file)
/////
//////////////////////////////////////////////////////////////////////////////////////////

    static public function readFileToArray($file)
    {
        if (!is_file($file)) return (false);

        $rv = file($file);
        return ($rv);
    }


//////////////////////////////////////////////////////////////////////////////////////////
/////
///// uploadExcelfile ($saveDir)
/////
//////////////////////////////////////////////////////////////////////////////////////////

    static public function uploadExcelFile($saveDir = "./imports/")
    {
        $files = (isset($_FILES)) ? $_FILES : false;

        $allowedExts = array("txt", "xls", "xlsx", "csv");
        $extension   = pathinfo($files["file"]["name"], PATHINFO_EXTENSION);

        if (($files["file"]["size"] > 2 * 1024 * 1024))
        {
            _Debug::error("File size to too large, it must be smaller than 2MB.", "File upload");
            return (false);
        }

        if (!in_array($extension, $allowedExts))
        {
            _Debug::error("Invalid file type. Your file can only be of the following types: " . strtoupper(implode(", ", $allowedExts)), "File upload");
            return (false);
        }

        if ($files["file"]["error"] > 0)
        {
            echo "Return Code: " . $files["file"]["error"] . "<br />";
        }
        else
        {
            echo "Upload: " . $files["file"]["name"] . "<br />";
            echo "Type: " . $files["file"]["type"] . "<br />";
            echo "Size: " . ($files["file"]["size"] / 1024) . " Kb<br />";
//        echo "Temp file: " . $files["file"]["tmp_name"] . "<br />";

            if ($files['userfile']['size'] > 0)
            {
                $fileName = pathinfo($files['userfile']['name'], PATHINFO_BASENAME);
                $tmpName  = $files['userfile']['tmp_name'];
                $fileSize = $files['userfile']['size'];
                $fileType = $files['userfile']['type'];

                $fp      = fopen($tmpName, 'r');
                $content = fread($fp, filesize($tmpName));
                $content = addslashes($content);
                fclose($fp);

                if (!get_magic_quotes_gpc())
                {
                    $fileName = addslashes($fileName);
                }

                $savedFile = $saveDir . $fileName;

                move_uploaded_file($tmpName, $savedFile);
                if (is_file($savedFile))
                {
                    echo $fileName . " has been stored. <br/>" . "\n";
                    return ($fileName);
                }
                else
                {
                    echo "Failure to store {$fileName} <br/>" . "\n";
                    return (false);
                }

            }
        }

        return (false);
    }
// *******************************************************************************************************************************************
//
//  uploadFile($saveDir="./imports/", $allowedExts=array("html"))
//
// *******************************************************************************************************************************************
    static public function uploadFile($saveDir = "./imports/", $allowedExts = array("html"))
    {
        if (!is_array($allowedExts)) $allowedExts = array($allowedExts);
        $uniqueList  = $allowedExts;
        $allowedExts = array_merge($allowedExts, array_map('strtoupper', $allowedExts), array_map('strtolower', $allowedExts));  // extend valid extensions to include upper and lower case options

        if (!isset($_FILES) || count($_FILES) == 0) return (false);

        $files = (isset($_FILES)) ? $_FILES : false;

        if (!isset($files["userfile"])) _Debug::error($_FILES, "files");

        $extension = strtolower(pathinfo($files["userfile"]["name"], PATHINFO_EXTENSION));
        _Debug::whereAmI();

        $megs = 2;
        if (($files["userfile"]["size"] > $megs * 1024 * 1024))
        {
            _Debug::error("File size to too large, it must be smaller than {$megs}MB.", "File upload");
            return (false);
        }

        if (count($files) > 0 && empty($files['name']))
        {
            if (!empty($extension) && !in_array($extension, $allowedExts))
            {
                _Debug::error("Invalid file type. Your file can only be of the following types: " . strtoupper(implode(", ", $uniqueList)), "File upload");
                return (false);
            }

            if ($files["userfile"]["error"] > 0)
            {
                echo "Return Code: " . $files["userfile"]["error"] . "<br />";
            }
            else
            {
                if ($files['userfile']['size'] > 0)
                {
                    $fileName = pathinfo($files['userfile']['name'], PATHINFO_BASENAME);
                    $tmpName  = $files['userfile']['tmp_name'];
                    $fileSize = $files['userfile']['size'];
                    $fileType = $files['userfile']['type'];

                    $fp      = fopen($tmpName, 'r');
                    $content = fread($fp, filesize($tmpName));
                    $content = addslashes($content);
                    fclose($fp);

                    if (!get_magic_quotes_gpc())
                    {
                        $fileName = addslashes($fileName);
                    }

                    $savedFile = $saveDir . $fileName;
                    move_uploaded_file($tmpName, $savedFile);
                    if (is_file($savedFile))
                    {
                        _Debug::notice($fileName . " has been uploaded.");
                        return ($savedFile);
                    }
                    else
                    {
                        echo "Failure to store {$fileName} <br/>" . "\n";
                        return (false);
                    }

                }
            }
        }
        else
        {
            _Debug::alert("No file was selected to upload.");
            _Debug::c($files, 'files');
        }

        return (false);
    }

// *******************************************************************************************************************************************
//
//  uploadFiles($saveDir="./imports/", $allowedExts=array("html"))
//
// *******************************************************************************************************************************************
    static public function uploadFiles($formField, $allowedExts = array("html"), $parameters = array())
    {
        $defaults = array("savePath" => "./uploads/", "newName" => false, "timestampName" => false, "numberMultiples" => true, "echoStats" => false);

        $par = wh_Lib::assignDefaults($parameters, $defaults);

        if (!is_array($allowedExts)) $allowedExts = array($allowedExts);
        $uniqueList  = $allowedExts;
        $allowedExts = array_merge($allowedExts, array_map('strtoupper', $allowedExts), array_map('strtolower', $allowedExts));  // extend valid extensions to include upper and lower case options

        if (!isset($_FILES) || count($_FILES) == 0) return (false);

        $files = $_FILES;

        if (empty($formField)) return (false);

        if (!isset($files[$formField])) return (false);

        $file = $files[$formField];

        $uploads = array();
        if (is_array($file['name']))
        {
            foreach ($file['name'] as $i => $t)
            {
                $tmp       = array();
                $v         = 'name';
                $tmp[$v]   = $file[$v][$i];
                $v         = 'tmp_name';
                $tmp[$v]   = $file[$v][$i];
                $v         = 'type';
                $tmp[$v]   = $file[$v][$i];
                $v         = 'size';
                $tmp[$v]   = $file[$v][$i];
                $v         = 'error';
                $tmp[$v]   = $file[$v][$i];
                $uploads[] = $tmp;
            }
        }
        else
        {
            $tmp       = array();
            $v         = 'name';
            $tmp[$v]   = $file[$v];
            $v         = 'tmp_name';
            $tmp[$v]   = $file[$v];
            $v         = 'type';
            $tmp[$v]   = $file[$v];
            $v         = 'size';
            $tmp[$v]   = $file[$v];
            $v         = 'error';
            $tmp[$v]   = $file[$v];
            $uploads[] = $tmp;
        }

        $uploadedFiles = array();
        foreach ($uploads as $idx => $file)
        {

            switch ($file["error"])
            {
                case UPLOAD_ERR_OK: // 0
                    break;
                case UPLOAD_ERR_NO_FILE: // 4
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE: // 1
                case UPLOAD_ERR_FORM_SIZE: // 2
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            $extension = strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));

            $megs = 20;
            if (($file["size"] > $megs * 1024 * 1024))
            {
                _Debug::error("`{$file['name']}` is too large, it must be smaller than {$megs}MB.", "File upload");
                return (false);
            }

            if (!empty($extension) && !in_array($extension, $allowedExts))
            {
                _Debug::error("Invalid file type: {$extension}. Your file can only be of the following types: " . strtoupper(implode(", ", $uniqueList)), "File upload");
                return (false);
            }

            if ($file["error"] > 0)
            {

                switch ($file["error"])
                {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new RuntimeException('No file sent.');
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        $pms = ini_get('post_max_size');
                        $umf = ini_get('upload_max_filesize');
                        throw new RuntimeException("Exceeded filesize limit of {$pms}.");
                    default:
                        throw new RuntimeException('Unknown errors.');
                }
            }
            else
            {
                if ($file['size'] > 0)
                {
                    $fileName = pathinfo($file['name'], PATHINFO_FILENAME);
                    if ($par['newName']) $fileName = $par['newName'];
                    if ($par['timestampName']) $fileName .= "_" . date("Y-m-d_H-i-s");
                    if ($par['numberMultiples']) $fileName .= "_" . str_pad($idx + 1, 3, "0", STR_PAD_LEFT);
                    $fileName .= ".{$extension}";

                    if (substr($par['savePath'], -1) != "/") $par['savePath'] .= "/";

                    $tmpName  = $file['tmp_name'];
                    $fileSize = $file['size'];
                    $fileType = $file['type'];

                    if (!get_magic_quotes_gpc()) $fileName = addslashes($fileName);

                    $savedFile = $par['savePath'] . $fileName;

                    move_uploaded_file($tmpName, $savedFile);
                    if (is_file($savedFile))
                    {
                        _Debug::notice($fileName . " has been uploaded.");
                        $uploadedFiles[] = $savedFile;

                        if ($par['echoStats'])
                        {
                            echo "<div class='info-box' style='position: relative; margin: 20px; padding:20px; border: 1px solid #9AF;'>";
                            echo "Uploaded: " . $file["name"] . "<br />";
                            if ($par['newName']) echo "Saved as: " . $fileName . "<br />";
                            echo "Type: " . $fileType . "<br />";
                            echo "Size: " . ($fileSize / 1024) . " Kb<br />";
                            echo "</div>";
                        }
                    }
                    else
                    {
                        echo "Failure to store {$fileName} <br/>" . "\n";
                        if ($par['echoStats'])
                        {
                            echo "<div class='info-box' style='position: relative; margin: 20px; padding:20px; border: 1px solid #fA9;'>";
                            echo "Requested File: " . $file["name"] . "<br />";
                            echo "Type: " . $fileType . "<br />";
                            echo "Size: " . ($fileSize / 1024) . " Kb<br />";
                            echo "</div>";
                        }
                        return (false);
                    }
                }
            }
        }

        return ($uploadedFiles);
    }


//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function writeArrayToString($ay)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function writeArrayToString($ay, $name = "", $func = "_unknown_")
    {
        $retval = "";
        foreach ($ay as $key => $val)
        {
            $retval .= $key . " = " . $val;
            $retval .= "\n";
            if (is_array($val))
            {
                $retval .= "[" . $key . "]";
                $retval .= "\n";
                foreach ($val as $k => $v)
                {
                    $retval .= "      " . $k . " = " . $v;
                    $retval .= "\n";
                }
            }
        }
        return ($retval);
    }

//////////////////////////////////////////////////////////////////////////////////////////
/////
///// function writeToLog($ay)
/////
//////////////////////////////////////////////////////////////////////////////////////////
    static public function writeToLog(&$var, $func = "_unknown_", $logFile = null)
    {
        if (empty($logFile)) $logFile = _Get::SV('logFile)');

        $fh = @fopen($logFile, "a")
        or die ("Can't open file (and your little dog too.");

        fputs($fh, _Get::SV('pCode') . " => " . $func . " ===>");

        if (is_array($var))
        {
            foreach ($var as $key => $val)
            {
                fputs($fh, "[" . $key . "] => " . $val);
                fputs($fh, "\r\n");
                if (is_array($val))
                {
                    foreach ($val as $k => $v)
                    {
                        fputs($fh, "      [" . $k . "] => " . $v);
                        fputs($fh, "\r\n");
                    }
                }
            }
        }
        else
        {
            fputs($fh, getSV('pCode') . " => " . $func . " => " . $var);
            fputs($fh, "\r\n");
        }
        fclose($fh);
    }

}