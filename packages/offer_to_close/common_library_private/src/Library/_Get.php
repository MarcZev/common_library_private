<?php

namespace offer_to_close\common_library_private\Library;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use \RecursiveIteratorIterator;
use \RecursiveDirectoryIterator;

/*******************************************************************************
 * Class _Get
 *
 * Author: Marc Zev
 * Development Date: Dec 19, 2011
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/

class _Get extends Toumai
{

    private static $Request = array();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $string
     *
     * @return string
     */
    public static function cleanString($string)
    {
        $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);

        return ($string);
    }

//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    subArrayWithWildCard ($needle, $haystack)
//
// 		return array of elements that contain the (case insensitive) needle as any part of the array element value.
//              I.e. if $needle = 'bob' elements with 'bobcat', 'bobbin', 'Karl Bob Marx', and 'Joe Bob' would all be returned.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

    /**
     * @param      $needle
     * @param      $haystack
     * @param bool $searchKey
     *
     * @return mixed
     */
    public static function  subArrayWithWildCard ($needle, $haystack, $searchKey=false)
    {
        $retVal = $haystack;
        $match = [];

//        $regex = "(?i){$needle}(?-i)";
        $regex = "/{$needle}/";

        if ($searchKey)
        {
/* todo
            foreach ($haystack as $index => $value)
            {
                preg_grep($regex, $retVal);
                if (empty($retVal)) continue;
                $match[$index] = $value;
            }
*/
        }
        else
        {
                preg_grep($regex, $retVal);
        }

        return ($retVal);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param        $dir
     * @param string $search
     *
     * @return array|bool
     */
    public static function files($dir, $search = "*")
    {
        if (empty($dir)) return (false);

        $dir = str_replace("//", "/", $dir . "/");

        $top = glob($dir . $search, GLOB_MARK);
        if ($key = array_search(".", $top) !== false) unset($top[$key]);
        if ($key = array_search("..", $top) !== false) unset($top[$key]);

        foreach ($top as $key => $file)
        {
            $f = substr($file, -1);
            if ($f == "\\") unset($top[$key]);  // remove directories from list
        }

        return ($top);

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $name
     *
     * @return bool
     */
    public static function icon($name)
    {
        $imgDir = self::dir('icons') . $name;
        $srch   = $imgDir . "*.*";

        $list = glob($srch, GLOB_MARK);
        foreach ($list as $key => $file)
        {
            $f = substr($file, -1);
            if ($f == "\\") continue;  // skip directories from list

            if (strtolower(pathinfo($file, PATHINFO_FILENAME)) == strtolower($name)) return ($file);
        }
        return (false);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $name
     *
     * @return bool
     */
    public static function image($name)
    {
        $imgDir = self::dir('images') . $name;
        $srch   = $imgDir . ".*";

        $list = glob($srch, GLOB_MARK);
        foreach ($list as $key => $file)
        {
            $f = substr($file, -1);
            if ($f == "\\") continue;  // skip directories from list

            if (strtolower(pathinfo($file, PATHINFO_FILENAME)) == strtolower($name)) return ($file);
        }
        return (false);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $file
     *
     * @return bool
     */
    public static function imageSize($file)
    {
        if (!is_file($file) || !_Files::isImage($file)) return (false);

        $tmp = getimagesize($file);

        if (!$tmp) return (false);

        $rv['width']         = $tmp[0];
        $rv['height']        = $tmp[1];
        $rv['imageRatioWoH'] = $tmp[0] / $tmp[1];
        $rv['imageRatioHoW'] = $tmp[1] / $tmp[0];
        $rv['imageType']     = $tmp[2];

        return ($rv);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Input a month number (1-12) and the full name of the month is returned.
     *
     * @param $month
     *
     * @return false|null|string
     */
    public static function monthName($month)
    {
        if (!is_numeric($month) || $month < 1 || $month > 12) return null;

        return date("F", mktime(1, 1, 1, $month, 1, date("Y")));
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param int $length
     *
     * @return string
     */
    public static function randomNumber($length = 5)
    {
        $pass = "";

// possible number chars.
        $chars = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

        for ($i = 0; $i < $length; $i++)
        {
            shuffle($chars);
            $pass .= $chars[0];
        }

        return $pass;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param int $length
     *
     * @return string
     */
    public static function randomPassword($length = 8, $withSymbols=false)
    {
        $pass = '';

// possible password chars. Note: Several letters are missing because they can be misinterpreted. The missing letters
// are uppercase I, O (eye & oh), and lowercase i, l (eye & el)

        $chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                       'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                       'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            ];
        $symbols = ['<', '>', '?', '*', '&', '&', '%', '$', '#', '@', '=', '+',];

        $bank = $withSymbols ? array_merge($chars, $symbols) : $chars;
        for ($i = 0; $i < $length; $i++)
        {
            shuffle($bank);
            $pass .= $bank[0];
            if ($pass == 0 ) $pass = array_rand(array_slice($bank, 1));
        }

        return $pass;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param       $root
     * @param array $extList
     * @param array $skipDir
     *
     * @return array
     */
    public static function wholeDirTree($root, $extList = array(), $skipDir = array())
    {
        $extFlag = (count($extList) > 0) ? true : false;

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );


        $paths = array($root);
        foreach ($iter as $path => $dir)
        {
            if ($extFlag && !in_array($dir->getExtension(), $extList)) continue;

            $d      = $dir->getPath();
            $skipIt = false;
            foreach (explode('/', $d) as $v)
            {
                if (in_array($v, $skipDir)) $skipIt = true;
            }
            if ($skipIt) continue;
            $f = $dir->getFilename();

            if (!isset($paths[$d])) $paths[$d] = array();
            if (!in_array($f, $skipDir)) $paths[$d][] = $f . (($dir->isDir()) ? '/' : null);
        }

        ksort($paths);
        return ($paths);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     *
     * @alias of directory
     *
     * @param $key
     *
     * @return mixed
     */
    public static function dir($key)
    {
        return self::directory($key);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $key
     *
     * @return bool
     */
    public static function directory($key)
    {
        $list = Config::get('constants.DIRECTORIES');

        return $list($key) ?? false;
    }

    /**
     * @return mixed
     */
    public static function logFlag()
    {
        return Config::get('constants.LOG_ON');
    }

    /**
     * @return mixed
     */
    public static function debugFlag()
    {
        return Config::get('constants.DEBUG_ON');
    }
}