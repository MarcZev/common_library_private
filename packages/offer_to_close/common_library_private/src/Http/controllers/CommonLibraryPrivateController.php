<?php           // offer_to_close\common_library_private\src\Http\Controllers\CommonLibraryPrivateController.php
namespace offer_to_close\common_library_private\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonLibraryPrivateController extends Controller
{

    public function splash()
    {
        return view('common_library_private::splash');
    }
}